var filterCount = 0; //This is the index used to label the latest filter. New filters are given an index one more than the last created filter, even if some of the previous ones have been deleted.
var filterIndexes = new Array();
var labels = [];
XNAT.app.filterUI = {};
var dataDictionaryJSON = new Object();
populateDataDictionary();
//removed filterMap, it was being built before the keys were loaded from the data tables

var isLoadingGroup = 0;//This is a flag that is set to 1 when a group begins loading. It is justed to prevent the data tables from flashing after each individual filter is added.

//add passthrough method which will save the current filters before loading postFilterAction
XNAT.app.filterUI.postFiltersActionPreSave = function () {
    if(!($('#downloadButton').hasClass('disabled'))){
        XNAT.app.dashboard.onLogFilterUpdate.subscribe(postFiltersAction);
        XNAT.app.dashboard.logFilter(XNAT.app.HcpUtil.getUserMostRecentDownloadGroupName());//this will save the current filter on the server (and trigger the onLogFilterUpdate event)
    }
}

var postFiltersAction = function () {
    var parameters = XNAT.app.dashboard.buildFilterMap();
    parameters["viewId"] = XNAT.app.dashboard.getSubjectViewId();
    parameters["query"] = XNAT.app.dashboard.buildFilterPseudoQuery();

    // We have to do it this way instead of creating the form on the fly because Firefox is dumb.
    var form = document.getElementById("filtersActionForm");

    var input, parameter;
    for (parameter in parameters) {
        input = document.createElement("input");
        input.type = "hidden";
        input.name = XNAT.app.dashboard.translateFilterKey(parameter, parameters["viewId"]);
        input.value = parameters[parameter];
        form.appendChild(input);
    }

    var csrf = document.createElement("input");
    csrf.type = "hidden";
    csrf.name = "XNAT_CSRF";
    csrf.value = csrfToken;
    form.appendChild(csrf);

    form.submit();

    return true;
};

function succ(o) {
    dataDictionaryJSON = jQuery.parseJSON(o.responseText);
}

function populateDataDictionary() {
    var cb = {
        success: succ,
        failure: function (o) {},
        cache : false // Turn off caching for IE
    };
    var json = YAHOO.util.Connect.asyncRequest('GET', serverRoot + '/REST/services/ddict/attributes', cb, null);
}

function getSelectedLabel(filterIndex) {
    return document.getElementById("selectFilter" + filterIndex).value;
}

function getSelectedValue(filterIndex) {
    var operator = document.getElementById("selectOperator" + filterIndex).value;
    var val = "";
    if (operator == "'NULL'") {
        val += "'NULL'";
    }
    else if (operator == "'NOT NULL'") {
        val += "'NOT NULL'";
    }
    else if (operator == "=") {
        var regExpression = new RegExp(",", "g"); //Turn all commas into |s.
        val += "=" + document.getElementById("selectValue" + filterIndex).value.replace(regExpression, "|");
    }
    else if (operator == "!=") {
        var regExpression = new RegExp(",", "g"); //Turn all commas into |s.
        val += "!=" + document.getElementById("selectValue" + filterIndex).value.replace(regExpression, "|");
    }
    else if (operator == ">") {
        val += ">" + document.getElementById("selectValue" + filterIndex).value;
    }
    else if (operator == "<") {
        val += "<" + document.getElementById("selectValue" + filterIndex).value;
    }
    else if (operator == ">=") {
        val += ">=" + document.getElementById("selectValue" + filterIndex).value;
    }
    else if (operator == "<=") {
        val += "<=" + document.getElementById("selectValue" + filterIndex).value;
    }
    else if (operator == "BETWEEN") {
        val += document.getElementById("selectValue" + filterIndex + "a").value + "--" + document.getElementById("selectValue" + filterIndex + "b").value;
    }
    else if (operator == "CONTAINS") {
        val += "CONTAINS" + document.getElementById("selectValue" + filterIndex).value;
    }
    else if (operator == "!CONTAINS") {
        val += "!CONTAINS" + document.getElementById("selectValue" + filterIndex).value;
    }
    return val;
}

function validateFilter(filterIndex) {
    var isValid = true;
    var key = document.getElementById("selectFilter" + filterIndex).value;
    var valRegExp = getDictEntry(key).validation;
    var currOperator = document.getElementById("selectOperator" + filterIndex).value;
    if (currOperator == "BETWEEN") {
        if (valRegExp != null && valRegExp != undefined) {
            var arrMatchesA = (document.getElementById("selectValue" + filterIndex + "a").value).match(valRegExp);
            var arrMatchesB = (document.getElementById("selectValue" + filterIndex + "b").value).match(valRegExp);
            isValid = (arrMatchesA != null && arrMatchesA.length > 0 && arrMatchesB != null && arrMatchesB.length > 0);
        }
    }
    else if (document.getElementById("selectValue" + filterIndex) != null) {
        if (valRegExp != null && valRegExp != undefined) {
            if(currOperator=="!=" || currOperator=="="){
                var valuesArr = document.getElementById("selectValue" + filterIndex).value.split(/[,|]+/);
                for(var valArrCounter = 0;valArrCounter<valuesArr.length;valArrCounter++){
                    var arrMatches = (valuesArr[valArrCounter]).match(valRegExp);
                    isValid = (arrMatches != null && arrMatches.length > 0);
                    if(!isValid){//If anything in the list fails validation, it fails validation
                        return isValid;
                    }
                }
            }
            else{
                var arrMatches = (document.getElementById("selectValue" + filterIndex).value).match(valRegExp);
                isValid = (arrMatches != null && arrMatches.length > 0);
            }
        }
    }
    return isValid;
}

function applyFilter(filterIndex) {
    if(!($('#addNewFilter').is(":visible"))){
        if (validateFilter(filterIndex)) {
            //Filter satisfies regular expression validation.

            var alreadyExistingFiltersForSelectedAttribute = XNAT.app.dashboard.filters[getSelectedLabel(filterIndex)];
            if(alreadyExistingFiltersForSelectedAttribute!=undefined && alreadyExistingFiltersForSelectedAttribute!=null && alreadyExistingFiltersForSelectedAttribute.indexOf(getSelectedValue(filterIndex))>-1){
                //An identical filter already exists.
                showMessage("page_body", "Duplicate Filter", "An identical filter has already been applied. Please select a different filter to add.");
            }
            else{
                //The filter does not yet exist and should be added.
                XNAT.app.dashboard.numActiveFilters++;
                if($('#selectValue'+filterIndex).parents('.selector').length>0){
                    //Add a tooltip to the dropdown that contains the text of the option selected, not its value.
                    $('#selectValue'+filterIndex).parents('.selector').prop('title',document.getElementById("selectValue" + filterIndex).options[document.getElementById("selectValue" + filterIndex).selectedIndex].text);
                }
                if($('#selectValue'+filterIndex).parents('.filter-text').length>0){
                    $('#selectValue'+filterIndex).parents('.filter-text').prop('title',document.getElementById("selectValue" + filterIndex).value);
                }

                $('#addNewFilter').show();
                $('#addNewFilter').visible
                XNAT.app.dashboard.addFilter(getSelectedLabel(filterIndex), getSelectedValue(filterIndex));
                $('#filter' + filterIndex).find('.filter-row').removeClass('active').addClass('filtered');
                $('#filter' + filterIndex).find('.filter-remove').remove();


                $('.filter-row').not('#addNewFilter').each(function (i) {
                    var j = $(this).find('#index').val();
                    $(this).append('<p class="filter-edit"><a href="javascript:void(0)" onclick="editFilter(' + j + ');" class="edit">Edit</a> <a href="javascript:void(0)" onclick="deleteFilter(' + j + ');" class="remove">Remove</a></p>');
                });


                $('#filter' + filterIndex).find('input').prop('disabled', 'disabled');
                $('#filter' + filterIndex).find('select').prop('disabled', 'disabled');
                $('#filter' + filterIndex).find('.filter-button').hide();
                $('#filter' + filterIndex).find('.filter-remove').remove(); 	// removes the temporary "Cancel" link
                $('.filter-edit').show(); 	// shows all hidden "Edit / Remove" links
            }
        }
        else {
            var key = document.getElementById("selectFilter" + filterIndex).value;
            var mess = getDictEntry(key).validationMessage;
            if (mess != null && mess != undefined) {
                showMessage("page_body", "Invalid Filter", mess);
            }
            else {
                showMessage("page_body", "Invalid Filter", "The value you entered did not match the expected pattern for this data type. Please try again.");
            }
        }
    }
}

function editFilter(filterIndex) {
    try {
        XNAT.app.dashboard.removeFilter(getSelectedLabel(filterIndex), getSelectedValue(filterIndex));
    } catch (e) {
    }

    if($('#selectValue'+filterIndex).parents('.selector').length>0){
        $('#selectValue'+filterIndex).parents('.selector').prop('title','');
    }
    if($('#selectValue'+filterIndex).parents('.filter-text').length>0){
        $('#selectValue'+filterIndex).parents('.filter-text').prop('title','');
    }

    $('#filter' + filterIndex).find('.filter-row').addClass('active');
    $('#filter' + filterIndex).find('select').prop('disabled', false);
    $('#filter' + filterIndex).find('input[type="text"]').prop('disabled', false);

    $('.filter-edit').remove();	// find "Edit / Remove" links and kill it

    var applyButton = $('#filter' + filterIndex).find('#addFilter' + filterIndex);

    $(applyButton).show().find('input').prop('disabled', false);

    $('#filter' + filterIndex).find('.filter-row').append('<p class="filter-remove"><a href="javascript:void(0)" onclick="deleteFilter(' + filterIndex + ');" class="remove">Remove</a></p>');
    $('#addNewFilter').hide(); // hides "Add new filter" button
}

function deleteFilter(filterIndex) {
    try {
        XNAT.app.dashboard.removeFilter(getSelectedLabel(filterIndex), getSelectedValue(filterIndex));
    } catch (e) {
    }

    $('#filter' + filterIndex).remove();
    filterIndexes.splice(filterIndexes.indexOf(filterIndex), 1);
    $('#addNewFilter').show(); // reveals "Add new filter" button
    $('.filter-edit').remove();

    $('.filter-row').not('#addNewFilter').each(function (i) {
        var j = $(this).find('#index').val();
        $(this).append('<p class="filter-edit"><a href="javascript:void(0)" onclick="editFilter(' + j + ');" class="edit">Edit</a> <a href="javascript:void(0)" onclick="deleteFilter(' + j + ');" class="remove">Remove</a></p>');
    });
}

function newFilter() {
    $('#addNewFilter').hide();
    filterIndexes[filterCount] = filterCount + 1;
    filterCount++;
    //  $('.newFilter').remove();
    $('.filter-edit').remove();

    // define variable for new filter container (should be global?)
    var filterSet = '<div class="filter-container" id="filter' + filterCount + '" name="filter' + filterCount + '">';
    filterSet += '<div class="filter-row active"><input type="hidden" id="index" value="' + filterCount + '" />';
    filterSet += '<div class="selector"><select id="selectCategory' + filterCount + '" name="selectCategory' + filterCount + '" onchange="selectCategory(' + filterCount + ')"><option value="">Select Category</option></select></div>';
    filterSet += '<div class="selector disabled"><select id="selectInstrument' + filterCount + '" name="selectInstrument' + filterCount + '" onchange="selectInstrument(' + filterCount + ')" disabled><option value="">Select Assessment</option></select></div>';
    filterSet += '<div class="selector disabled"><select id="selectFilter' + filterCount + '" name="selectFilter' + filterCount + '" onchange="selectFilter(' + filterCount + ')" disabled><option value="">Select Attribute</option></select></div>';
    filterSet += '<div class="selector disabled"><select id="selectOperator' + filterCount + '" name="selectOperator' + filterCount + '" onchange="selectOperator(' + filterCount + ')" disabled><option value="">Select Operator</option></select></div>';
    filterSet += '<p class="filter-remove"><a href="javascript:void(0)" onclick="deleteFilter(' + filterCount + ');" class="remove">Remove</a></p>';
    filterSet += '</div>';
    filterSet += '</div>';

    // Add new filter container directly above the "Add new filter" container
    $('.filters').append(filterSet);
    for (var index = 0; index < categories.length; index++) {
        var selectCategory = YUIDOM.get('selectCategory' + filterCount);
        selectCategory.options[selectCategory.options.length] = new Option(categories[index], categories[index]);
    }

    return filterCount;
}

function populateInstruments(categoryName, filterIndex) {
    currIndex = filterIndex;
    var instrumentArray = new Array();
    for (var key in attributeMap[categoryName]) {
        instrumentArray.push(key);
    }
    var selectIns = YUIDOM.get('selectInstrument' + filterIndex);
    for (var insCounter=0; insCounter<instrumentArray.length;insCounter++) {
        var currInsName = instrumentArray[insCounter];
        selectIns.options[selectIns.options.length] = new Option(currInsName,currInsName);
    }

}

function clear(o) {
    if(o!=null){
        o.selectedIndex = 0;
        $(o).parents('.selector').prop('title','');
        $(o).parents('.selector').addClass('disabled');
        $(o).prop('disabled', 'disabled');
        while (o.options.length > 1) {
            o.remove(1);
        }
    }
}

function selectCategory(filterIndex) {
    clear(document.getElementById('filterForm').elements.namedItem("selectFilter" + filterIndex));
    clear(document.getElementById('filterForm').elements.namedItem("selectInstrument" + filterIndex));
    clear(document.getElementById('filterForm').elements.namedItem("selectOperator" + filterIndex));
    $('.value' + filterIndex).parents('.selector').remove();
    $('.value' + filterIndex).parents('.filter-between').remove();
    $('.value' + filterIndex).parents('.filter-text').remove();
    $('#filter' + filterIndex).find('.filter-button').remove();

    var selectedCategory = document.getElementById("selectCategory" + filterIndex).value;
	$('#selectCategory'+filterIndex).parents('.selector').prop('title',selectedCategory);
    $('#selectInstrument'+filterIndex).parents('.selector').removeClass('disabled');
    $('#selectInstrument'+filterIndex).prop('disabled', false);
    populateInstruments(selectedCategory, filterIndex);
}

function selectInstrument(filterIndex) {
    clear(document.getElementById('filterForm').elements.namedItem("selectFilter" + filterIndex));
    clear(document.getElementById('filterForm').elements.namedItem("selectOperator" + filterIndex));
    $('.value' + filterIndex).parents('.selector').remove();
    $('.value' + filterIndex).parents('.filter-between').remove();
    $('.value' + filterIndex).parents('.filter-text').remove();
    $('#filter' + filterIndex).find('.filter-button').remove();

    var selectedCategory = document.getElementById("selectCategory" + filterIndex).value;
    var selectedInstrument = document.getElementById("selectInstrument" + filterIndex).value;
	$('#selectInstrument'+filterIndex).parents('.selector').prop('title',selectedInstrument);
    $('#selectFilter'+filterIndex).parents('.selector').removeClass('disabled');
    $('#selectFilter'+filterIndex).prop('disabled', false);
    populateFiltersDropdown(selectedCategory, selectedInstrument, filterIndex);
}

function selectFilter(filterIndex) {
    clear(document.getElementById('filterForm').elements.namedItem("selectOperator" + filterIndex));
    $('.value' + filterIndex).parents('.selector').remove();
    $('.value' + filterIndex).parents('.filter-between').remove();
    $('.value' + filterIndex).parents('.filter-text').remove();
    $('#filter' + filterIndex).find('.filter-button').remove();

    var filterSelect = document.getElementById("selectFilter" + filterIndex);
    var optionSelected = $('#selectFilter'+filterIndex).find('option:selected').html(); // should get the contents of the selected option tag.

    var selectedRow = filterSelect.selectedIndex;
    var selectedKey = filterSelect.value;
    $('#selectFilter'+filterIndex).parents('.selector').prop('title',optionSelected);
    $('#selectOperator'+filterIndex).parents('.selector').removeClass('disabled');
    $('#selectOperator'+filterIndex).prop('disabled', false);
    if (selectedRow >= 1) {
        var selectedFilterType = XNAT.utils.find(labels, selectedKey, function (lbl, key) {
            return (lbl[0] == key)
        })[2];
        populateOperatorsDropdown(filterIndex);
    }
}

function onTextboxKeyPress(e, filterIndex) {
    if (e.keyCode == 13) {//On enter, apply filter.
        applyFilter(filterIndex);
        return false;
    }
}

function selectOperator(filterIndex) {
    // remove the "Remove" link from the end of the filter row. We'll stick a new one on the end when we're done.
    $('#filter' + filterIndex).find('.filter-remove').remove();
    var selectedOp = document.getElementById("selectOperator" + filterIndex).value;
    if (selectedOp) {
        $('.value' + filterIndex).parents('.selector').remove();
        $('.value' + filterIndex).parents('.filter-between').remove();
        $('.value' + filterIndex).parents('.filter-text').remove();
        $('#filter' + filterIndex).find('.filter-button').remove();
        var vals = "";
        var selVal = document.getElementById("selectFilter" + filterIndex).value;
        vals = getDictEntry(selVal).values;

        var optionSelected = document.getElementById("selectOperator" + filterIndex).value;
        $('#selectOperator'+filterIndex).parents('.selector').prop('title',document.getElementById("selectOperator" + filterIndex).options[document.getElementById("selectOperator" + filterIndex).selectedIndex].text);

        //I refactored this code a bit to eliminate all fo the copy/paste sections.
        if (["=", "!=", ">", "<", ">=", "<=", "CONTAINS", "!CONTAINS"].indexOf(selectedOp) > -1) {
            if (vals != undefined) {
                $('#filter' + filterIndex).find('.filter-row').append('<div class="selector"><select id="selectValue' + filterIndex + '" class="value' + filterIndex + '" name="selectValue' + filterIndex + '" ="enableIfSelected(' + filterIndex + ');"><option value="">Select Value</option></select></div>');
                populateValuesDropdown(filterIndex);
                $('#filter' + filterIndex).find('.filter-row').append('<div class="filter-button" id="addFilter' + filterIndex + '"><input type="button" name="add' + filterIndex + '" id="add' + filterIndex + '" class="value' + filterIndex + '" onclick="applyFilter(' + filterIndex + ')" value="Apply" /></div><p class="filter-remove"><a href="javascript:void(0)" onclick="deleteFilter(' + filterIndex + ');" class="remove">Remove</a></p>');
            }
            else {
                $('#filter' + filterIndex).find('.filter-row').append('<div class="filter-text"><input id="selectValue' + filterIndex + '" type="text" class="value' + filterIndex + '" name="filter-key" value="" onkeypress="return onTextboxKeyPress(event,' + filterIndex + ')"/><span class="value' + filterIndex + '"/></div>');
                var key = document.getElementById("selectFilter" + filterIndex).value;
                var watermark = getDictEntry(key).watermark;
                if(watermark!=undefined && watermark!=null && watermark!=""){
                    $("#selectValue" + filterIndex).Watermark(getDictEntry(key).watermark);
                }
                $('#filter' + filterIndex).find('.filter-row').append('<div class="filter-button" id="addFilter' + filterIndex + '"><input type="button" name="add' + filterIndex + '" id="add' + filterIndex + '" class="value' + filterIndex + '" onclick="applyFilter(' + filterIndex + ')" value="Apply" /></div><p class="filter-remove"><a href="javascript:void(0)" onclick="deleteFilter(' + filterIndex + ');" class="remove">Remove</a></p>');
            }
        }
        else if (selectedOp == "BETWEEN") {
            $('#filter' + filterIndex).find('.filter-row').append('<div class="filter-between"><input id="selectValue' + filterIndex + 'a" type="text" class="value' + filterIndex + '" name="selectValue' + filterIndex + 'a" value="" onkeypress="return onTextboxKeyPress(event,' + filterIndex + ')"/><span class="value' + filterIndex + '">AND</span><input style="float:right;" id="selectValue' + filterIndex + 'b" type="text" class="value' + filterIndex + '" name="selectValue' + filterIndex + 'b" value="" onkeypress="return onTextboxKeyPress(event,' + filterIndex + ')"/></div>');
            $('#filter' + filterIndex).find('.filter-row').append('<div class="filter-button" id="addFilter' + filterIndex + '"><input type="button" name="add' + filterIndex + '" id="add' + filterIndex + '" class="value' + filterIndex + '" onclick="applyFilter(' + filterIndex + ')" value="Apply" /></div><p class="filter-remove"><a href="javascript:void(0)" onclick="deleteFilter(' + filterIndex + ');" class="remove">Remove</a></p>');
        }
        else if (["'NULL'", "'NOT NULL'"].indexOf(selectedOp) > -1) {
            $('#filter' + filterIndex).find('.filter-row').append('<div class="filter-button" id="addFilter' + filterIndex + '"><input type="button" name="add' + filterIndex + '" id="add' + filterIndex + '" class="value' + filterIndex + '" onclick="applyFilter(' + filterIndex + ')" value="Apply" /></div><p class="filter-remove"><a href="javascript:void(0)" onclick="deleteFilter(' + filterIndex + ');" class="remove">Remove</a></p>');
        }
    }
    else {
        $('.value' + filterIndex).remove();
    }
}

//function checkBetween(filterIndex){
//    var valA = document.getElementById("selectValue"+filterIndex+"a").value;
//    var valB = document.getElementById("selectValue"+filterIndex+"b").value;
//    if(isInt(valA) && isInt(valB) && (valA<=valB)){
//        enableAdd(filterIndex);
//    }
//    else{
//        disableAdd(filterIndex);
//    }
//}

function enableIfSelected(filterIndex) {
    if (document.getElementById("selectValue" + filterIndex).value == "") {
        disableAdd(filterIndex);
    }
    else {
        enableAdd(filterIndex);
    }
}

function enableIfNonEmpty(filterIndex) {
    if (document.getElementById("selectValue" + filterIndex).value == "") {
        disableAdd(filterIndex);
    }
    else {
        enableAdd(filterIndex);
    }
}

function isInt(value) {
    if ((undefined === value) || (null === value) || ("" === value)) {
        return false;
    }
    return value % 1 == 0;
}

function enableAdd(filterIndex) {
    $('input[id=add' + filterIndex + ']').removeAttr('disabled');
}

function disableAdd(filterIndex) {
    $('input[id=add' + filterIndex + ']').attr('disabled', 'disabled');
}

function getDictEntry(key) {
    return XNAT.utils.find(dataDictionaryJSON, key, function (dictEntry, k) {
        return (dictEntry.key == k);
    });
}

function populateValuesDropdown(filterIndex) {
    var key = document.getElementById("selectFilter" + filterIndex).value;
    var vals = getDictEntry(key).values;
    j = 0;
    for (var val in vals) {
        document.getElementById("selectValue" + filterIndex).options[j + 1] = new Option(vals[val], val);
        j++;
    }
}

function populateOperatorsDropdown(filterIndex) {
    var key = document.getElementById("selectFilter" + filterIndex).value;
    var ops = getDictEntry(key).operators;
    j = 0
    for (var op in ops) {
        document.getElementById("selectOperator" + filterIndex).options[j + 1] = new Option(ops[op], op);
        j++;
    }
}

XNAT.app.filterUI.catagoriesUpdated = new YAHOO.util.CustomEvent("filter-change", this);

//populate groups in the hidden dropdown list
XNAT.app.filterUI.populateGroups = function (groups) {
    var gSelect = $('#group_selector');
    gSelect.html('');//clear any previous groups

    gSelect.append($('<option></option>').prop("value", XNAT.app.dashboard.constants.GROUP_LIST_DEFAULT_OPTION).text(XNAT.app.dashboard.constants.GROUP_LIST_DEFAULT_OPTION));
    gSelect.append($('<option></option>').prop("value", "").text("-------------"));

    for (var pflC = 0; pflC < XNAT.app.dashboard.previousFiltersLog.length; pflC++) {
        var logEntry = XNAT.app.dashboard.previousFiltersLog[pflC];
        gSelect.append($('<option class="group"></option>').prop("value", logEntry.filter).prop("groupName", logEntry.desc).text(logEntry.desc.getExcerpt(60)));
        if (logEntry.isSiteFilter && pflC + 1 < XNAT.app.dashboard.previousFiltersLog.length && !XNAT.app.dashboard.previousFiltersLog[pflC + 1].isSiteFilter) {
            gSelect.append($('<option></option>').prop("value", "").text("-------------"));
        }
    }
    XNAT.app.filterUI.populateGroupsCompleted.fire();
}

XNAT.app.filterUI.populateGroupsCompleted = new YAHOO.util.CustomEvent("populateGroupsCompleted", this, false, YAHOO.util.CustomEvent.LIST, true);

XNAT.app.filterUI.showGroups = function () {
    $('#group_selector_div').show();
    $('#group_selector').focus();
}

XNAT.app.filterUI.hideGroups = function () {
    $('#group_selector_div').hide();
}

//values used in the operator field (0:displayed term, 1:value (usually prepended to value))
var operator_defs = [
    ["IS EMPTY", "'NULL'"],
    ["IS NOT EMPTY", "'NOT NULL'"],
    ["=", "="],
    ["!=", "!="],
    [">", ">"],
    ["<", "<"],
    [">=", ">="],
    ["<=", "<="],
    ["NOT =", "!="],
    ["CONTAINS", "CONTAINS"],
    ["DOESN'T CONTAIN", "!CONTAINS"]
];
XNAT.app.filterUI.identifyOperator = function (filter) {
    for (var odC = 0; odC < operator_defs.length; odC++) {
        if (filter.indexOf(operator_defs[odC][1]) == 0) {
            return operator_defs[odC];
        }
    }

    if (filter.indexOf("--") > -1) {
        return ["BETWEEN", "BETWEEN"];
    } else {
        return ["EQUALS", "EQUALS"]
    }
}

//retrieves the category id by matching the key to a column within that category
XNAT.app.filterUI.getCategoryByKey = function (key) {
    for (var catEntry2 in XNAT.app.filterUI.categoryMap) {
        var catFields = XNAT.app.filterUI.categoryMap[catEntry2];
        if (XNAT.utils.find(catFields, key, function (catF, key) {
            return catF[0] == key
        }) != null) {
            return catEntry2;
        }
    }

    return null;
}

//opens a preexisting group, clears any existing filters, loads the filter entries in the UI and applies them
XNAT.app.filterUI.openGroup = function (group, groupName) {

	group = XNAT.app.dashboard.translateFiltersFromGenericString(group);
	
	isLoadingGroup = 1;//Set the loading flag to true.

    $('#group_selector').blur();
    $('#group_selector_div').hide();

    //clear existing filters
    XNAT.app.dashboard.resetStoredFilters();
    for (var fcC = window.filterCount; fcC >= 0; fcC--) {
        deleteFilter(fcC);
    }
    XNAT.app.dashboard.numActiveFilters = 0;
    //re-apply filters
    for (var key in group) {//key is the actual column key
        var category = XNAT.app.filterUI.getCategoryByKey(key);

        if (category == null) {
            showMessage("page_body", "Error", "Unable to load category for field: " + key);
            return;
        }

        var gValues = group[key].split(",");//multiple filters on the same column will be merged in filter storage logic (so we need to re-separate them)

        for (var gVC = 0; gVC < gValues.length; gVC++) {
            var filterCount = newFilter();

            var insName = "";
            for (var filCounter=0; filCounter<dataDictionaryJSON.length;filCounter++) {
                if(key==dataDictionaryJSON[filCounter]["key"]){
                    insName= dataDictionaryJSON[filCounter]["assessment"];
                }
            }

            $('#selectCategory' + filterCount).val(category);
            selectCategory(filterCount);

            $('#selectInstrument' + filterCount).val(insName);
            selectInstrument(filterCount);

            $('#selectFilter' + filterCount).val(key);
            selectFilter(filterCount);

            var operatorA = XNAT.app.filterUI.identifyOperator(gValues[gVC]);

            $('#selectOperator' + filterCount).val(operatorA[1]);
            selectOperator(filterCount);

            //calculate value field
            var val = null;
            if (["EQUALS", "BETWEEN"].indexOf(operatorA[1]) > -1) {
                val = gValues[gVC];
            } else if (["'NULL'", "'NOT NULL'"].indexOf(operatorA[1]) > -1) {
                //leave null
            } else {
                val = gValues[gVC].substring(operatorA[1].length);
            }

            if (val != null) {
                if (["BETWEEN"].indexOf(operatorA[1]) > -1) {
                    var betweenArray = val.split("--");
                    $('#selectValue' + filterCount + 'a').val(betweenArray[0]);
                    $('#selectValue' + filterCount + 'b').val(betweenArray[1]);
                }
                else {
                    $('#selectValue' + filterCount).val(val);
                }
            }
            applyFilter(filterCount);
        }
    }
    if(groupName) {
    	XNAT.app.dashboard.setCurrentGroup(groupName);
    }
    else {
    	// anonymous filter
    	XNAT.app.dashboard.setCurrSubjDisplay();
    }

	isLoadingGroup = 0;//Set the loading flag to false.

    //After all the filters in the group have been loaded, update the tables in all the tabs.
    for (var dTsC3 = 0; dTsC3 < XNAT.app.dashboard.DTs.length; dTsC3++) {
        var dt = XNAT.app.dashboard.DTs[dTsC3];
        dt.dataTable.getPage(1);
    }
}

XNAT.app.filterUI.autoOpenGroup = function(groupName) {
    	isLoadingGroup = 1;
	var that = this;
	this.populateGroupsCompleted.subscribe(function() {
		var groupFilter;
		$("#group_selector option").each(function() {
			if($(this).prop("groupName") === groupName) {
				groupFilter = $(this).val();
			}
		});
		that.openGroup(groupFilter, groupName);
	});
}

XNAT.app.filterUI.autoOpenSubject = function(subject) {
    isLoadingGroup = 1;
	var that = this;
	this.populateGroupsCompleted.subscribe(function() {
		that.openGroup("&xnat:subjectData.SUBJECT_LABEL==" + subject);
	});
}

XNAT.app.filterUI.saveGroup = function () {
	var currentGroup = null;
	if(XNAT.app.dashboard.isCurrentFilterAGroup() && !XNAT.app.dashboard.currentGroup.isSiteFilter) {
		// passing in null for site filters, since we don't want them saving a filter under that name
		// (but you can "Save As" a different name)
		currentGroup = XNAT.app.dashboard.currentGroup;
	}
    XNAT.app.filterUI.filterLabelEditor.show(currentGroup);
}

XNAT.app.filterUI.deleteGroup = function () {
    if (confirm("Are you sure you want to delete this group?")) {
        XNAT.app.dashboard.deleteFilter();
        XNAT.app.dashboard.showAllSubjects();
    }
}

//when the group selector is modified, open that group
$("#group_selector").change(function () {
    $('#group_selector_div').hide();
    $('#group_selector').blur();

    var oFilter = $(this).val();

    if ($(this).find(":selected").hasClass("group")) {
        XNAT.app.filterUI.openGroup(oFilter, $(this).find(":selected").prop("groupName"));
    }
})

$("#group_selector").blur(function () {
    $('#group_selector_div').hide();
})

XNAT.app.filterUI.categoryMap = {};
XNAT.app.dashboard.unmatchedCols=new Array();
//when the dashboard is done loading... do this... WARNING: This will rerun everytime the tables are reloading (filter changes/ page changes)
XNAT.app.dashboard.onLoadComplete.subscribe(function () {

    try {
        var subjectsString = ((XNAT.app.dashboard.getDTByLabel("Subject Information").dataTable.initResults.ResultSet.totalRecords == 1) ? "1 Subject, " : XNAT.app.dashboard.getDTByLabel("Subject Information").dataTable.initResults.ResultSet.totalRecords + " Subjects, ");
        var sessionsString = ((XNAT.app.dashboard.getDTByLabel("MR Sessions").dataTable.initResults.ResultSet.totalRecords == 1) ? "1 MR Session." : XNAT.app.dashboard.getDTByLabel("MR Sessions").dataTable.initResults.ResultSet.totalRecords + " MR Sessions.");
        if(XNAT.app.dashboard.getDTByLabel("Subject Information").dataTable.initResults.ResultSet.totalRecords==0){
            if(!($('#downloadButton').hasClass('disabled'))){
                $('#downloadButton').addClass('disabled');
            }
        }
        else{
            if($('#downloadButton').hasClass('disabled')){
                $('#downloadButton').removeClass('disabled');
            }
        }
        $('div.counts').replaceWith('<div class="counts">' + subjectsString + sessionsString + '</div>');

    } catch (e) {
    }

    //build labels array for page filters
    if (labels.length == 0) {//should only run once
        var table_columns = XNAT.app.dashboard.buildColumns();

        for (var ldC1 = 0; ldC1 < table_columns.length; ldC1++) {
            var tc = table_columns[ldC1];//these are the actual columns in the tables which are filterable

            //add it to the list of filterable labels
            var labelArray = [tc.key, tc.header, tc.type, tc.element_name, tc.field_id];

            //find the matching dictionary entry for this column
            var dictEntry = XNAT.utils.find(dataDictionaryJSON, labelArray, function (dictF, labelF) {
                return(labelF[3] == dictF.xsiType && labelF[4] == dictF.fieldId);//make sure fields match discovered labels (match by element_name and field_id)
            });

            if (dictEntry != null) {
                //columns should not show in filter UI if they aren't in the data dictionary

                labelArray[5]=dictEntry.dictName;

                labels.push(labelArray);

                //set the table key on the local JSON entry, for easy access elsewhere
                dictEntry.key = tc.key;

                if (XNAT.app.filterUI.categoryMap[dictEntry.category] == null) {
                    XNAT.app.filterUI.categoryMap[dictEntry.category] = new Array();
                }

                //add label array to list of labels for categories
                XNAT.app.filterUI.categoryMap[dictEntry.category].push(labelArray);

                XNAT.app.dashboard.filterHeaders[tc.key] = tc.header;//this should probably be done somewhere in dashboard.js... but I'm not sure what it is doing.
            }else{
            	XNAT.app.dashboard.unmatchedCols.push(tc);
            }

        }

        //when the list of previous filters changes, do this.
        XNAT.app.dashboard.onHistoricalFiltersLoad.subscribe(XNAT.app.filterUI.populateGroups);

        //load the list of previous filters
        XNAT.app.dashboard.loadFiltersLog();

        newFilter();

        window.tab_manager.tabView.set('activeIndex', 0);

        window.tab_manager.onDTcreate.subscribe(function (o, args, me) {
            var dt = args[0];
            XNAT.app.dashboard.DTs.push(dt);

            //refresh the known columns any time a new data table is created, required for loading tables after initial table load
            dt.onTableInit.subscribe(function () {
                XNAT.app.dashboard.buildColumns()
            });
        });
    }
});


//modified to populate filters off of an in memory map of categories/fields built off of the inital dictionary load.
//this is required, when selecting the filters during filter reloads, the operation shouldn't have to wait on a REST call to populate the filters, when we already have the info in memory
function populateFiltersDropdown(categoryName, instrumentName, filterIndex) {
    currIndex = filterIndex;
    var catEntry = XNAT.app.filterUI.categoryMap[categoryName];
    var filterJSON = attributeMap[categoryName][instrumentName];
    var selectFilter = YUIDOM.get('selectFilter' + filterIndex);
    if(filterJSON!=null){
        for (var key in filterJSON) {
            for (var ceC = 0; ceC < catEntry.length; ceC++) {
                if(catEntry[ceC][4]==filterJSON[key]["fieldId"]){
                    selectFilter.options[selectFilter.options.length] = new Option(catEntry[ceC][5], catEntry[ceC][0]);
                }
            }
        }
    }

}


//////
//override implementations of the functions that add and remove real-time filters from the merged search xml

xdat_criteria.prototype.isColumnFilter = function (element_name, field_id, value) {
    if (this.getSchemaField() == element_name + "." + field_id) {
        var operatorA = XNAT.app.filterUI.identifyOperator(value);

        if ("BETWEEN" == operatorA[1]) {
            if (this.getComparisonType() == " BETWEEN " && this.getValue() == value.replace("--", " AND ")) {//xdat uses AND to represent ranges, filter UI uses --
                return true;
            }
        } else if ("EQUALS" == operatorA[1]) {
            if (this.getComparisonType() == "=" && this.getValue() == value) {
                return true;
            }
        } else if ("'NULL'" == operatorA[1]) {//the value in the xml will not have quotes
            if (this.getComparisonType() == "IS" && this.getValue() == "NULL") {
                return true;
            }
        } else if ("'NOT NULL'" == operatorA[1]) {//the value in the xml will not have quotes
            if (this.getComparisonType() == "IS" && this.getValue() == "NOT NULL") {
                return true;
            }
        } else if ("!CONTAINS" == operatorA[1]) {//the xml doesn't support CONTAINS (only LIKE)
            if (this.getComparisonType() == "NOT LIKE" && this.getValue() == value.substring(operatorA[1].length)) {
                return true;
            }
        } else if ("CONTAINS" == operatorA[1]) {//the xml doesn't support CONTAINS (only LIKE)
            if (this.getComparisonType() == "LIKE" && this.getValue() == value.substring(operatorA[1].length)) {
                return true;
            }
        } else {
            if (this.getComparisonType() == operatorA[1] && this.getValue() == value.substring(operatorA[1].length)) {
                return true;
            }
        }
    }
    return false;
}

xdat_criteria_set.prototype.addColumnFilter = function (element_name, field_id, value) {
    for (var csC = 0; csC < this.Criteria.length; csC++) {
        var cs = this.Criteria[csC];
        if (cs.isColumnFilter(element_name, field_id, value)) {
            return;
        }
    }

    var newC = new xdat_criteria();
    newC.setSchemaField(element_name + "." + field_id);

    var operatorA = XNAT.app.filterUI.identifyOperator(value);
    if ("BETWEEN" == operatorA[1]) {
        newC.setComparisonType(" BETWEEN ");
        newC.setValue(value.replace("--", " AND "));
    } else if ("EQUALS" == operatorA[1]) {
        newC.setComparisonType("=");
        newC.setValue(value);
    } else if ("'NULL'" == operatorA[1]) {//the value in the xml will not have quotes
        newC.setComparisonType("IS");
        newC.setValue("NULL");
    } else if ("'NOT NULL'" == operatorA[1]) {//the value in the xml will not have quotes
        newC.setComparisonType("IS");
        newC.setValue("NOT NULL");
    } else if ("!CONTAINS" == operatorA[1]) {//the xml doesn't support CONTAINS (only LIKE)
        newC.setComparisonType("NOT LIKE");
        newC.setValue(value.substring(operatorA[1].length));
    } else if ("CONTAINS" == operatorA[1]) {//the xml doesn't support CONTAINS (only LIKE)
        newC.setComparisonType("LIKE");
        newC.setValue(value.substring(operatorA[1].length));
    } else if ("=" == operatorA[1] && value.indexOf("|") > -1) {
        newC.setComparisonType("IN");
        value = value.substring(operatorA[1].length).replace(/[\|]/g, ',');//the xml uses , as the separator
        newC.setValue(value);
    } else if ("!=" == operatorA[1] && value.indexOf("|") > -1) {
        newC.setComparisonType("NOT IN");
        value = value.substring(operatorA[1].length).replace(/[\|]/g, ',');//the xml uses , as the separator
        newC.setValue(value);
    } else {
        newC.setComparisonType(operatorA[1]);
        newC.setValue(value.substring(operatorA[1].length));
    }

    this.addCriteria(newC);
}