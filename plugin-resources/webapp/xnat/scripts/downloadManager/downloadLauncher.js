/*jslint browser: true, white: true, vars: true */
/*global XNAT, AW, jQuery, showMessage */
function AsperaDownloadLauncher() {"use strict";

	this.launchDownload = function(url, sessionName) {

		var asperaWeb = new AW.Connect({
			id : 'aspera_web_transfers'
		});

		var fileControls = {
			'connect_settings' : {
				'allow_dialogs' : 'yes'
			}
		};
		fileControls.handleTransferEvents = function(event, obj) {
			switch (event) {
				case 'transfer':
					//document.getElementById('progress_meter').innerHTML = JSON.stringify(obj, null, 4);
					break;
			}
		};

		fileControls.downloadFile = function(el) {
			document.getElementById('transfer_spec').innerHTML = JSON.stringify(fileControls.transfer_spec, null, "    ");
			var response = asperaWeb.startTransfer(fileControls.transfer_spec, fileControls.connect_settings);
		};

		jQuery.post(url, '', function(data) {
			fileControls.transfer_spec = data;
			asperaWeb.initSession(sessionName);
			asperaWeb.addEventListener('transfer', fileControls.handleTransferEvents);
			fileControls.downloadFile(this);
		}).error(function(jqxhr, textStatus, errorThrown) {
			showMessage("page_body", "Error", errorThrown);
		});
	};
}
