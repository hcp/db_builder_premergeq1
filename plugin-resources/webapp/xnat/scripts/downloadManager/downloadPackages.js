/*
    Javascript for downloadPackages
*/


// define data formats here
var data_formats = ["preprocessed","unprocessed"];
// use the first one as default (or override manually)
var default_format = data_formats[0] ;


$(document).ready(function() {

    var $body = $('body');

    // main vars
    var
        all_formats = [],
        all_keywords = [],
        active_keywords = [],
        active_formats = [default_format], // the default format needs to be active and matched on load
        match_formats = [default_format],  // ---------------------------------------------------------
        match_keywords = [],
        count_active_formats = 1, // there is one default format and it is active
        count_active_keywords = 0,
        total_packages = 0
    ;

    // jQuery objects
    var $packages_container = $('#packages_container');
    var $packages = $('#packages');

    var $total_packages = $('.totals .total_packages') ;
    var $total_count = $('.totals .total_count');
    var $total_size = $('.totals .total_size');

    // match type selector
    var $keyword_match = $('#keyword_match');
    $keyword_match.find('.any').prop('selected', true);

    function packagesTotals(){

        var total_selected = 0,
            total_hidden = 0,
            total_count = 0,
            total_size = 0 ;

        var pkg_count = 0,
            pkg_size = 0 ;

        var total_matched = $('.package.matched').size();
        //var total = $('.package.selected.matched');

        $('.package.selected').each(function(){

            var pkg_count = parseInt($(this).find('.pkg_count').attr('data-count-pkg'), 10);
            var pkg_size = parseInt($(this).find('.pkg_size').attr('data-size-pkg'), 10);

            total_count = total_count + pkg_count ;
            total_size = total_size + pkg_size ;

            total_selected++ ;

            // if a package *doesn't* have a "matched" class,
            // add it to the count of hidden packages
            if (!($(this).hasClass('matched'))){
                total_hidden++ ;
            }

        });

        $total_packages.attr('data-count-packages', total_selected).text(addCommas(total_selected));
        $total_count.attr('data-count-total', total_count).text(addCommas(total_count));
        $total_size.attr('data-size-total', total_size).text(sizeFormat(total_size));

        if (total_selected === 1){
            $total_packages.next('.plural').hide();
        }
        else {
            $total_packages.next('.plural').show();
        }
        if (total_count === 1){
            $total_count.next('.plural').hide();
        }
        else {
            $total_count.next('.plural').show();
        }

        if (total_count > 0) {
            $('.buttons .download, .buttons .reset').removeAttr('disabled');
        } else {
            $('.buttons .download, .buttons .reset').attr('disabled','disabled');
        }

        if (total_hidden > 0){
            $('.totals .hidden_packages').text('' +
                ' (' + total_hidden + ' hidden)' +
            '');
        }
        else {
            $('.totals .hidden_packages').text('');
        }

        if (total_matched === 0){
            $('#no_match').show();
        }
        else {
            $('#no_match').hide();
        }

        if ((total_matched === total_selected) || total_matched === 0){
            $('.buttons .select_all').attr('disabled','disabled');
        }
        else {
            $('.buttons .select_all').removeAttr('disabled');
        }

    }

    function resetMatches() {
        $('.package').each(function(){
            $(this).removeClass('matched');
            if ($(this).find('.formats .format.active').length){
                $(this).addClass('matched');
            }
        });
        //$('.package').addClass('matched').show();
        $('.keywords .keyword').removeClass('active');
        $('.keywords .show_all').hide();
        active_keywords = [] ;
        match_keywords = [] ;
        //count_active_formats = 0 ;
        count_active_keywords = 0 ;
    }

    function filterPackageList() {
        $('.package').each(function() {

            match_formats = [] ;
            match_keywords = [];

            var $this_package = $(this);

            $this_package.find('.keywords .keyword').each(function() {
                var this_keyword = $(this).text();
                if (active_keywords.indexOf(this_keyword) > -1) {
                    match_keywords.push(this_keyword);
                }
            });

            $this_package.find('.formats .format').each(function() {
                var this_format = $(this).text();
                if (active_formats.indexOf(this_format) > -1) {
                    match_formats.push(this_format);
                }
            });

            if ((match_formats.length) === count_active_formats){
            if ($keyword_match.val() === "any") {
                if (match_keywords > []) {
                        $this_package.addClass('matched');
                }
                    else {
                        $this_package.removeClass('matched');
            }
                }
            if ($keyword_match.val() === "all") {
                if ((match_keywords.length) === count_active_keywords) {
                        $this_package.addClass('matched');
                }
                    else {
                        $this_package.removeClass('matched');
            }
                }

            }
            else {
                $this_package.removeClass('matched');
            }

        });

        if (active_keywords > []){
            $('.show_all').show();
        }
        else {
            $('.show_all').hide();
            resetMatches() ;
        }

    }


    var csvSubjects = $('input[name="subject"]').map(function(){
        return $(this).val();
    }).get().join(',');

    // ///////////////// //
    // load the packages //
    // ///////////////// //
	openModalPanel("DownloadPackagesModal", "Loading data...");
    $.ajax({
        type : "POST",
        url : serverRoot + "/spring/download?subjects=" + csvSubjects + "&view=packages&XNAT_CSRF=" + csrfToken,
        data : "",
        dataType : "json",
        context : this,
        success : function(json) {

            // if the formats are defined as an array in the returned json
            //
            // like:
            /*
             "formats":[
                "processed",
                "RAW"
             ],
             */
            //json.formats.map(function(i) {
            //    data_formats.push(i);
            //    default_format = data_formats[0] ;
            //});

            $.each(json.packages, function() {

                var my_formats = [];
                var my_keywords = [] ;

                total_packages++ ;

                var
                    pkg_id    = this.id,
                    pkg_label = this.label,
                    pkg_desc  = this.description,
                    pkg_count = this.count,
                    pkg_size  = this.size,
                    pkg_class,
                    subjs_ok  = this.subjects_ok,
                    subjs_all = this.subjects_total,
                    keywords  = this.keywords
                    ;

                // add each packages keywords to the "my_keywords" array
                keywords.map(function(i) {
                    // if it's not in the "data_formats" array, it's a regular keyword
                    if (data_formats.indexOf(i) === -1) {
                        my_keywords.push(i);
                        // add unique keywords to the "all_keywords" array
                        if (all_keywords.indexOf(i) === -1){
                            all_keywords.push(i);
                        }
                    }
                    else {
                        my_formats.push(i);
                        if (all_formats.indexOf(i) === -1){
                            all_formats.push(i);
                        }
                    }
                });

                pkg_class = (subjs_ok === 0) ? "package disabled" : "package" ;

                // make packages using default format "matched" on initial load
                if (my_formats.indexOf(default_format) > -1){
                    pkg_class += " matched" ;
                }

                // write each package to the page inside #packages
                $('#packages').append(

                    '<div class="'+pkg_class+'" id="' + pkg_id + /* '" package-label="' + pkg_label + */ '" data-keywords="' + my_keywords.join(' ') + '">' +  //
                    '   <input type="hidden" class="match_count" value="0"> '+
                    '   <a href="javascript:" class="pkg_icon add_to_queue"><img src="' + serverRoot + '/style/images/_.gif" width="52" height="52" alt="package" title="add to queue"></a>' + //
                    '   <div class="pkg_top">' + //
                    '       <h5><a href="javascript:" class="package_title add_to_queue">'+pkg_label+'</a></h5>' + //
                    '       <div class="summaries">' + //
                    '           <span class="subjects_ok" data-subjects-ok="' + subjs_ok + '">' + addCommas(subjs_ok) + '</span> of <span class="subjects_all" data-subjects-all="' + subjs_all + '">' + addCommas(subjs_all) + '</span> subjects OK' + //
                    '           &nbsp; &ndash; &nbsp;' + //
                    '           <b><span class="pkg_count" data-count-pkg="' + pkg_count + '">' + addCommas(pkg_count) + '</span> files, <span class="pkg_size" data-size-pkg="' + pkg_size + '">' + sizeFormat(pkg_size) + '</span></b>' + //
                    '       </div>' + // <!-- /.summaries -->
                    '       <div class="clear"></div>' + //
                    '   </div>' + // <!-- /.pkg_top -->
                    '   <div class="pkg_bottom">' + //
                    '       <a href="javascript:" class="add_to_queue queue_button"><img src="' + serverRoot + '/style/images/_.gif" width="139" height="20" alt="queue" title="queue for download"></a>' +
                    '       <p class="pkg_desc">' + pkg_desc + '</p>' + //
                    '       <p class="keywords formats">format: <a href="javascript:" class="format">' + my_formats.join('</a> <a href="javascript:" class="format">') + '</a> <span style="margin:0 5px;color:#aaa;">|</span> modalities: <a href="javascript:" class="keyword">' + my_keywords.join('</a> <a href="javascript:" class="keyword">') + '</a></p>' + //
                    '       <a href="javascript:" class="expand_details">+</a>' + //
                    '       <div class="clear"></div>' + //
                    '   </div>' + // <!-- /.pkg_bottom -->

                    // ///// preview box ///// //

                    '   <div class="pkg_preview">' + //
                    '       <table class="" cellspacing="0" cellpadding="0">' + //
                    // removing the "Package Preview" header - kinda ugly, probably unnecessary
                    //'           <tr>' +  //
                    //'               <td>' + //
                    //'                   <table class="preview_title" cellspacing="0" cellpadding="0">' + //
                    //'                       <tr>' + //
                    //'                           <td align="left">Package Preview</td>' + //
                    //'                           <td align="right"><span class="expand_details">X</span></td>' + //
                    //'                       </tr>' + //
                    //'                   </table>' + // <!-- /.preview_title -->
                    //'               </td>' + //
                    //'           </tr>' + //
                    '           <tr>' + //
                    '               <td>' + //
                    '                   <table class="preview_details" cellspacing="0" cellpadding="0">' + //
                    '                       <tr class="labels">' + //
                    '                           <th align="left">Subject</th>' + //
                    '                           <th align="left">Status</th>' + //
                    '                           <th align="left">File Count</th>' + //
                    '                           <th align="left">Size</th>' + //
                    '                       </tr>' +  // <!-- ./labels -->

                    // ///// subject <tr> rows will go here ///// //

                    '                   </table>' + // <!-- /.preview_details -->
                    '               </td>' + //
                    '           </tr>' + //
                    '       </table>' + // (close outer table)
                    '   </div>' + // <!-- /.pkg_preview -->
                    '</div>' // <!-- /.package -->

                );

            });

            // create a button for each keyword for filtering
            all_keywords.map(function(i) {
                $('#all_keywords').find('span.keywords').append('<a href="javascript:" class="keyword button" id="keyword-'+i+'">'+i+'</a>');
            });

            all_formats.map(function(i){
//                if (data_formats.indexOf(i) > -1) {
                // make sure the default format is shown first
                if (default_format === i){
                    $('#all_keywords').find('span.formats').prepend('<a href="javascript:" class="format button active default" id="format-'+i+'">'+i+'</a>');
                }
                else {
                    $('#all_keywords').find('span.formats').append('<a href="javascript:" class="format button" id="format-'+i+'">'+i+'</a>');
                }
//                }
            });

            $('.formats .format:contains("' + default_format + '")').addClass('active');

			closeModalPanel("DownloadPackagesModal");
        },

        error : function(jqXHR) {

            //showMessage("page_body", "Error", "Error " + jqXHR.status + ": " + jqXHR.responseText);
            xModalOpen({
                kind: 'small' ,
                width: 0 ,
                height: 0 ,
                box: 'error_modal' ,
                title: "Error" ,
                content: jqXHR.status + ": " + jqXHR.responseText ,
                footer:{
                    show: 'yes' ,
                    content: '<span class="buttons"><a href="javascript:" class="close default button">OK</a></span>'
                }
            });
        }

    });


    //var new_total_count = 0 ;
    $packages.on('click', '.add_to_queue', function() {
        var $this_package = $(this).closest('.package');
        // if it's not 'disabled' then add to queue
        if (!($this_package.hasClass('disabled'))){
            if ($this_package.hasClass('selected')){
                $this_package.removeClass('selected');
            }
            else {
                $this_package.addClass('selected');
            }
            $this_package.find('.add_to_queue').removeClass('hover');
            packagesTotals();
        }
        else {
            xModalOpen({
                kind: 'dialog',
                content: 'There are no valid subjects for this package.',
                footer: {
                    show: 'yes',
                    content: '<span class="buttons"><a class="close default button" href="javascript:">OK</a></span>'
                }//,
//                ok: false,       // false = no button, 'OK' = ok button with text "OK"
//                cancel: false,   // false = no button, 'Cancel' = cancel button with text "Cancel"
//                close: 'OK',     // if one button just needs to acknowledge there's a message
//                default: 'close' // default = 'ok','cancel','close'
            })
        }
    });


    $packages_container.on('click', '#all_keywords .keyword', function() {

        var the_keyword = $(this).text();

            if ($(this).hasClass('active')) {
                $('.keywords .keyword.active:contains("' + the_keyword + '")').removeClass('active');
                active_keywords.splice($.inArray(the_keyword, active_keywords), 1);
                count_active_keywords-- ;
            }
            else {
                //console.debug (count_active_keywords+' active keywords before: '+active_keywords);
                $('.keywords .keyword:contains("' + the_keyword + '")').addClass('active');
                if (active_keywords.indexOf(the_keyword) === -1) {
                    active_keywords.push(the_keyword);
                    count_active_keywords++ ;
                }
            }

        filterPackageList();
        packagesTotals();

    });

    $packages_container.on('click', '#all_keywords .format', function() {

        var the_format = $(this).text();

        $('.formats .format').removeClass('active');

        // toggle - one at a time!
        if (!($(this).hasClass('active'))) {
            $('.formats .format:contains("' + the_format + '")').addClass('active');
            active_formats = [];
            active_formats.push(the_format);
            count_active_formats = 1 ;
        }

        filterPackageList();
        packagesTotals();

    });



    $packages.on('mouseover','.add_to_queue',function(){
        $(this).closest('.package').find('.add_to_queue').addClass('hover');
    });
    $packages.on('mouseout','.add_to_queue',function(){
        $(this).closest('.package').find('.add_to_queue').removeClass('hover');
    });


    // show the subjects info on click if "i"
    $packages.on('click', '.expand_details', function() {

        var $this_pkg = $(this).closest('.package');
        var pkg_id = $this_pkg.attr('id');

        // if the subject preview is not loaded, get the stuff
        if (!($this_pkg.find('.pkg_preview').hasClass('loaded'))) {
            $.ajax({
                type : "POST",
                url : serverRoot + "/spring/download?package=" + pkg_id + "&subjects=" + csvSubjects + "&view=subjects&XNAT_CSRF=" + csrfToken,
                data : "",
                dataType : "json",
                context : this,
                success : function(json) {

                    $.each(json.subjects, function(){

                        var
                            sub_id     = this.id,
                            sub_status = this.status,
                            sub_count  = this.file_count,
                            sub_size   = this.size
                            ;

                        $this_pkg.find('.preview_details').append(

                            '<tr class="subject">' + //
                            '   <td class="label">' + sub_id + '</td>' + //
                            '   <td class="status">' + sub_status + '</td>' + //
                            '   <td class="count" data-count="' + sub_count + '">' + addCommas(sub_count) + ' files</td>' + //
                            '   <td class="size" data-size="' + sub_size + '">' + sizeFormat(sub_size) + '</td>' + //
                            '</tr>' //

                        );

                    });

                    $this_pkg.find('.pkg_preview').addClass('loaded').slideToggle('fast');

                },

                error : function(jqXHR) {
                    if(404 === jqXHR.status) {
                        //showMessage("page_body", "Notification", "There are no valid subjects for this package.");
                        xModalOpen({kind:'fixed',width:'300',height:'150',box:'notification',title:"Notification",content:'There are no valid subjects for this package.',footer:{show:'yes',content:'<span class="buttons"><a href="javascript:" class="close default button">OK</a></span>'}});
                    } else {
                        //showMessage("page_body", "Notification", "Error " + jqXHR.status + ": " + jqXHR.responseText);
                        var modal_content = "Error " + jqXHR.status + ": " + jqXHR.responseText ;
                        xModalOpen({kind:'small',box:'notification',title:"Notification",content:modal_content,footer:{show:'yes',content:'<span class="buttons"><a href="javascript:" class="close default button">OK</a></span>'}});
                    }
//                    $(this).text("+");
                }

            });

            if ($(this).hasClass('open')){
                $(this).removeClass('open');
            }
            else {
                $(this).addClass('open');
            }


        }
        // if the subject preview is already loaded, just show it
        else {
            $this_pkg.find('.pkg_preview').slideToggle('fast');
//            if ($(this).text() === "+"){
//                $(this).text("-");
//            }
//            else {
//                $(this).text("+");
//            }
            if ($(this).hasClass('open')){
                $(this).removeClass('open');
            }
            else {
                $(this).addClass('open');
            }
        }

    });



    // when the any/all pulldown is changed
    $keyword_match.change(function(){
        filterPackageList();
        packagesTotals();
    });


    $('.show_all').click(function(){
        resetMatches();
        packagesTotals();
    });


    $('.buttons .select_all').click(function(){
        $('.package.matched').addClass('selected');
        packagesTotals();
    });


    $('.buttons .reset').click(function() {
        $('.package.selected').removeClass('selected');
        $('.totals .total_count').attr('data-count-total', 0).text('0');
        $('.totals .total_size').attr('data-size-total', 0).text('0 B');
        packagesTotals();
    });



    $body.on('click','.download_packages_button',function() {
        var total_packages = parseInt($total_packages.attr('data-count-packages'), 10);
        var total_count = parseInt($total_count.attr('data-count-total'), 10);
        var total_size = parseInt($total_size.attr('data-size-total'), 10);
        var $package_selected = $('.package.selected/*.matched*/');
        var selected_packages = [];
        var selected_package_labels = [];
        if ($package_selected.length) {
            $package_selected.each(function() {
                var this_id = $(this).attr('id');
                var this_label = $(this).find('.package_title').text(); // using text from package label, no need for 'package-label' attribute
                selected_packages.push(this_id);
                selected_package_labels.push(this_label);
            });
            $("#packageIdCsv").val(selected_packages.join(','));

            var total_packages_display, total_files_display ;
            if (total_packages === 1){
                total_packages_display = '1 package' ;
            }
            else {
                total_packages_display = total_packages + ' packages' ;
            }
            if (total_count === 1){
                total_files_display = '1 file' ;
            }
            else {
                total_files_display = '<b style="font-weight:bold;color:#080;">' + addCommas(total_count) + ' total files</b>' ;
            }

            var dialog_content =

                "<h3>You've selected " + total_packages_display + " for download:</h3>" +
                '<ul class="package_list">' +
                '   <li>' +
                        selected_package_labels.join('</li><li>') +
                '   </li>' +
                '</ul>' +
                '<p>This download has ' + total_files_display + ', with a total size of <b style="font-weight:bold;color:#080;">' + sizeFormat(total_size) + '</b>.</p>' +
                //"<p><b>Proceed with download?</b></p>" +
                '';

            //confDownload("Confirm Download", "You've selected the " + selected_package_labels.join(', ') + " packages, containing " + addCommas(total_count) + " files, totalling " + sizeFormat(total_size) + ".\n\nProceed with download?");

            var footer_content =
                '<b style="display:block;position:absolute;left:2px;top:18px;font-weight:normal;font-size:14px;color:#666;">Proceed with download?</b> ' +
                '<span class="buttons right"><a href="javascript:" class="cancel button">Cancel</a> <a href="javascript:" class="ok default button">Download Now</a></span>' +
                '';

            xModalOpen({
                kind:     'fixed',  // size - 'fixed','custom','large','med','small',
                width:     550,  // box width when used with 'fixed' size, horizontal margin when used with 'custom' size
                height:    350,  // box height when used with 'fixed' size, vertical margin when used with 'custom' size
                scroll:   'yes', // does the content need to scroll? (yes/no)
                box:      'confirm',  // class name of this modal box
                title:    'Confirm Download', // text for modal title bar - if empty will be "Alert" - use space for blank title
                content:   dialog_content, // body content of modal window
                footer:    {
                    show: 'yes',
                    height: '' , // height in px - blank for default - 52px
                    background: '#f0f0f0', // css for footer background - white if blank
                    border: '#d0d0d0', // color for footer top border - white if blank
                    content: footer_content // empty string renders default footer with two buttons (ok/cancel) using values specified below
                },
                           // by default the buttons will go in the footer, this can be overridden by adding 'class="ok button"' and 'class="cancel button"' to the body content
                ok:       'Download Now', // text for submit button - if blank uses "OK"
                cancel:   'Cancel' // text for cancel button - if blank uses "Cancel"
            });


            xModalSubmit = function(){
                $("#downloadPackagesForm").submit();
                //alert('downloading the stuff');
                xModalClose();
            };

            xModalCancel = function(){
                // it's ok if this is blank - if you want to have a function if someone cancels, put it here
                xModalClose();
            };

        }
    });

    // show the filter criteria from previous page
    $('#subject_filters').find('a').click(function(){
        $(this).closest('div').find('ul').slideToggle('fast');
    });

});

$(window).load(function(){
    $('body').addClass('loaded');
    var loaded = true ;
    //console.debug('Loaded?');
    // hack so stupid IE9 will 'click' the default format when the page loads
//    window.defaultFormat = setInterval(function(){
//        if (loaded === true){ //check if everything is loaded
//            $('#all_keywords').find('.format.default').trigger('click');
//            clearInterval(window.defaultFormat);
//        }
//    },100);
    $('#loading_packages').delay(100).fadeOut(100);
    $('#all_keywords, #packages, #download_box').delay(100).fadeIn(200);
    //console.debug('formats: ' + data_formats.join(', ') + ';  default: ' + default_format);
});