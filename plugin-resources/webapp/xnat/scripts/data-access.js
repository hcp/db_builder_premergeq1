// JavaScript Document

/*
 * ---
 * general modal controls
 * ---
 */


function showRegDiv() {
	showModal('reg-div');
	validateRegForm();
}
function showModal(id) {
	$('#page-mask').show();
	$('#'+id).show();
	$('#'+id+' input').first().focus();
}
function showModal(id,showOpts) {
	if (showOpts) {
		$('div[name="interact-div"]').show();
		$('div[name="viewdut-div"]').hide();
	} else {
		$('div[name="interact-div"]').hide();
		$('div[name="viewdut-div"]').show();
	}
	$('#page-mask').show();
	$('#'+id).show();
	$('#'+id+' input').first().focus();
}
function modalClose(id) {
	$('#page-mask').hide();
	$('#reg-div div[name="interact-div"]').show();
	if (id) {
		$('#'+id).hide();
	} else {
		$('.modal').hide();
	}
}
function closeConfirmation() {
	if ($('#reg-confirm-msg').text().match(/Thank/)) {
		modalClose();
	} else {
		modalClose('reg-confirm-div');
		modalClose('reg-processing-div');
	}
}
/* 
 * --- 
 * E-mail/Password Modification
 * ---
 */
function ConfirmEmail() {
	if(YUIDOM.get('new_email').value == YUIDOM.get('confirm_email').value)
	{
		if(YUIDOM.get('new_email')!="" && YUIDOM.get('new_email').value.indexOf('@')>-1){
			return true;
		}else{
			alert("Please enter a valid email address");
			return false;
		}
	}else{
		alert('Values do not match');
		YUIDOM.get('new_email').value='';
		YUIDOM.get('confirm_email').value='';
		return false;
	}
}

function ConfirmPassword() {
	if (document.getElementById)
	{
		if(document.getElementById('new_password').value == document.getElementById('confirm_password').value)
		{
			return true;
		}else{
			alert('Values do not match');
			document.getElementById('new_password').value='';
			document.getElementById('confirm_password').value='';
			return false;
		}
	}
	else if (document.all)
	{
		if(document.all['new_password'].value == document.all['confirm_password'].value)
		{
			return true;
		}else{
			alert('Values do not match');
			document.all['new_password'].value='';
			document.all['confirm_password'].value='';
			return false;
		}
	}
}

/* 
 * --- 
 * Registration form checker 
 * ---
 */
$(document).ready(function(){
	// adds highlight to focused field, and highlights help text 
	$('.inputWrapper input').focusin( function(){
		$(this).parent().addClass('highlighted');
		$(this).next().removeClass('hidden');
	});
	// removes highlight and hides help text
	$('.inputWrapper input').focusout( function(){
		$(this).parent().removeClass('highlighted');
		$(this).next().addClass('hidden');
	});
	// validates username input 
	$('#reg-div input[name="username"]').focusout( function(){
		$(this).val($(this).val().toLowerCase());
	});
	// validates password complexity -- NEEDS FIXING
	$('#reg-div input[name="pw"]').focusout( function(){
		var string = $(this).val();
		//if ((string) && (string.match(/[a-z]/)) && (string.match(/[A-Z]/)) && (string.match(/(\d|\W)/)) && (string.length >= 8)) {
		if ((string) && (string.replace(/^[ ]*/,'').replace(/[ ]*$/,'').length >= 5)) {
			$('#password-simple').addClass('hidden');
			$(this).parent().removeClass('error');
		} else {
			$('#password-simple').removeClass('hidden');
			$(this).parent().addClass('error');
		}
		var matchedString = $('#reg-div input[name="pwc"]').val();
		if (matchedString.length>0) {
			if (string === matchedString) {
				$('#password-mismatch').addClass('hidden');
				$('#reg-div input[name="pwc"]').parent().removeClass('error');
			} else {
				$('#password-mismatch').removeClass('hidden');
				$('#reg-div input[name="pwc"]').parent().addClass('error');
			}
		}
	});
	// validates password match
	$('#reg-div input[name="pwc"]').focusout( function(){
		var string = $(this).val();
		var matchedString = $('#reg-div input[name="pw"]').val();
		if (string === matchedString) {
			$('#password-mismatch').addClass('hidden');
			$(this).parent().removeClass('error');
		} else {
			$('#password-mismatch').removeClass('hidden');
			$(this).parent().addClass('error');
		}
	});

	$('#new_password').focusout( function(){
		var string = $(this).val();
		if (string.length<1 || string.replace(/^[ ]*/,'').replace(/[ ]*$/,'').length >= 5) {
			$('#password-simple').addClass('hidden');
			if (string.length>1) {
				$('#pw_submit').removeAttr('disabled');
			}
		} else {
			if ($('#password-simple').hasClass('hidden')) {
				$(this).focus();
			}
			$('#password-simple').removeClass('hidden');
			$('#pw_submit').attr('disabled','disabled');
		}
	});
			
	// validates email input
	$('#new_email').focusout( function(){
		var string=$(this).val().toLowerCase();
		if ((string) && ( !string.match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/) )) {
			if ($('#email-error').hasClass('hidden')) {
				$(this).focus();
			}
			$(this).addClass('error');
			$('#email-error').removeClass('hidden');
			$('#email_submit').attr('disabled','disabled');
		} else {
			$('#email-error').addClass('hidden');
			if (string.match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/)) {
				$('#email_submit').removeAttr('disabled');
			}
		}
	});
			
	// validates email input
	$('#reg-div input.email').focusout( function(){
		var string=$(this).val().toLowerCase();
		if ((string) && ( !string.match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/) )) {
			$(this).parent().addClass('error');
			$('#email-error').removeClass('hidden');
		} else {
			$('#email-error').addClass('hidden');
			$(this).parent().removeClass('error');
		}
	});
	
	// validates form completion -- NEEDS FIXING
	$('.inputWrapper input').blur( function() {
		validateRegForm();
	});

    $('.inputWrapper input').keyup( function() {
        validateRegForm();
    });

    $('.inputWrapper input').bind('paste', function(e) {
        setTimeout(function() {//The timeout is necessary because otherwise the event is triggered before the text gets pasted.
            validateRegForm();
        }, 50);
    });
});

function validateRegForm() {
	var formComplete = true;
	$('.inputWrapper input.required').each(function() {
		var string=$(this).val();
		if (!string) {
			formComplete = false;
			return false;
		}
	});
	if (formComplete && !$('#reg-div div').is('.error')) {
		$('#regSubmit').removeClass('hidden');
		$('.required_text').addClass('hidden');
	} else {
		$('#regSubmit').addClass('hidden');
		$('.required_text').removeClass('hidden');
	}
}

/* 
 * --- 
 * Registration thank you and confirmation control 
 * ---
 */

function showThanks() {
	var username = $('#reg-div input[name="username"]').val();
	var email = ($('#reg-div input.email').val()) ? $('#reg-div input.email').val() : 'empty';
	
	$('#reg-thanks-msg').text('You have created an account with the username "'+ username +'." Please check your email at '+ email +' to complete your registration.');
	$('#reg-div').addClass("hidden");
	$('#reg-processing-div').addClass("hidden").hide();
	$('#reg-thanks-div .modal-title').text("Please confirm your registration");
	$('#reg-thanks-div').removeClass("hidden").show();
	return false;
}
function showConfirm() {
	var username = $('#regForm input[name="username"]').val();
	var email = ($('#reg-div input.email').val()) ? $('#regForm input.email').val() : 'empty';
	
//	$('#reg-confirm-msg').text('Thank you for registering an HCP account with the email address ' + email + '. You have created an account with the username "'+ username +'".');
	$('#reg-confirm-msg').text('You have created an account with the username "'+ username +'." Please check your email at '+ email +' to complete your registration.');
	$('#reg-div').addClass("hidden");
	$('#reg-confirm-div').removeClass("hidden").show();
	return false;
}
function showFailed(txt) {
	$('#reg-confirm-msg').text("").append(txt);
	$('#reg-div').addClass("hidden");
	$('#reg-confirm-div').removeClass("hidden").show();
	return false;
}
function dbRegisterCall(connectURI) {
	// Submit registration
	//$('#reg-div').addClass("hidden");
	$('#reg-processing-div').removeClass("hidden").show();
	$('#regSubmit').attr("disabled","disabled");
	var formObject = document.getElementById('regForm');
	YAHOO.util.Connect.setForm(formObject,false);
	YAHOO.util.Connect.asyncRequest('POST',connectURI,dbRegCallback,null);

}
dbRegCallback={
	success:function(o){
		dbRegSuccess(o);
			},
	failure:function(o){
		dbRegFailed(o);
	},
    cache : false, // Turn off caching for IE
	scope:this
};
function dbRegSuccess(o) {
	showConfirm();
	// Submit request to join hcp-announce list
	if ($('#hcp-announce').is(":checked")) {
	   $('#hcpAnnounceListEmail').val( $('#email').val() );
	   $form = $('#hcpAnnounceListForm');
	   $form.submit();
	}
	// Submit request to join hcp-users list
	if ($('#hcp-users').is(":checked")) {
	   $('#dataUsersListEmail').val( $('#email').val() );
	   $form = $('#dataUsersListForm');
	   $form.submit();
	}
	$('.inputWrapper input').val("");
}
function dbRegFailed(o) {
	if (o.responseText != null && o.responseText.length<100) {
		showFailed(o.responseText);
	} else {
		showFailed("The server could not process the registration");
	}
}

/* 
 * --- 
 * validate username / password reset form
 * currently set to disable form and highlight errors
 * ---
 */

$(document).ready(function(){
	// prevents entry in both form fields. 
	$('input#forgotUsername').focusin(function(){
		$('input#forgotPassword').val('');
	});
	$('input#forgotPassword').focusin(function(){
		$('input#forgotUsername').val('');
	});
});

function forgotValidation() {
	var mailUser=$("#forgotUsername").val(); 
	var resetPass=$("#forgotPassword").val();
	if (!mailUser && !resetPass) {
		alert("Please enter values");
		return false;
	}
/*
	if (mailUser) {
		$("#pass-reminder-error")
			.empty()
			.append("<strong>Error: </strong>The email address you entered cannot be found in the HCP database.")
			.removeClass("hidden");
		$("#forgotUsername")
			.focus();
	}
	if (resetPass) {
		$("#pass-reminder-error")
			.empty()
			.append("<strong>Error: </strong>The username you entered cannot be found in the HCP database.")
			.removeClass("hidden");
		$("#forgotPassword")
			.focus();
	}
	return false;
*/
	return true;
}

/* 
 * --- 
 * Data Use Terms controls 
 * ---
 */

 // TO DO: write script that enables checkbox when user scrolls down (i.e. "reads") the terms of use. 

function dutConfirm(ele) {
	/* 
	 * Change the page according to their acceptance of terms.
	 * Requires a page refresh. Add the DUT version accepted as a hash to the URL beforehand, so we can parse it and launch a modal.
	 */
	window.location.hash = ele; 
	window.location.reload(true); 
}
 
function agreeChange(checkbox,id) {
	// if user checks box, don't let them uncheck. Turn on the form submit and guide them to it. 
	$(checkbox).prop('disabled','disabled');
	$('#'+id).removeClass('hidden'); /* .removeAttr('disabled').focus() */
	
}

function dutError(ele,responseText) {
	/* 
	 * Change the page according to their acceptance of terms.
	 * Would this require a page refresh in XNAT?
	$('#dut-' + ele).addClass("hidden");
	$('#dut-thanks-div').removeClass("hidden").show();
	$('#reg-thanks-msg').text("").append("Could not register data use acceptance" + responseText );
	 */
	
}

function dutSubmit(terms) {
	
	$('#dut-processing-div').removeClass("hidden").show();
	if (terms == "Phase1") {
		$('#p1-btn-agree').attr("disabled","disabled");
		arg = ["Phase1PilotData"];
		params = 'terms=Phase1&acceptTerms=true&XNAT_CSRF='+window.csrfToken;
		YAHOO.util.Connect.asyncRequest('POST',serverRoot +'/REST/services/datause?'+params,{ success:duPostSuccess,failure:duPostFailure,argument:arg,cache:false });
	} else if (terms == "Phase2") {
		$('#p2-btn-agree').attr("disabled","disabled");
		arg = ["Phase2OpenAccess"];
		params = 'terms=Phase2&acceptTerms=true&XNAT_CSRF='+window.csrfToken;
		YAHOO.util.Connect.asyncRequest('POST',serverRoot +'/REST/services/datause?'+params,{ success:duPostSuccess,failure:duPostFailure,argument:arg,cache:false });
	} else if (terms == "WorkbenchData") {
		$('#wb-btn-agree').attr("disabled","disabled");
		arg = ["Phase2OpenAccess", "Phase2OpenAccess_GroupAverage"];
		params = 'terms=WB&acceptTerms=true&XNAT_CSRF='+window.csrfToken;
		YAHOO.util.Connect.asyncRequest('POST',serverRoot +'/REST/services/datause?'+params,{ success:duPostSuccess,failure:duPostFailure,argument:arg,cache:false });
	}

}

function duPostSuccess(o) {
	if (o.responseText.toUpperCase() == "TRUE" || o.responseText.toUpperCase() == "MOD") {
		dutConfirm(o.argument);
	} else {
		dutNotYet(o.argument,o.responseText);
	}
}

function duPostFailure(o) {
	dutError(o.argument,o.responseText);
}

function duGetSuccess(o) {
	var dutCheck = (o.responseText.toUpperCase() === "TRUE");
	if (o.responseText.toUpperCase() === "TRUE") {
		dutAlready(o.argument);
	} else if (o.responseText.toUpperCase() == "MOD") {
		window.location.reload(true); 
		return;
	} else {
		dutNotYet(o.argument,false);
	}
	receivedDutCheckResponse(o.argument, dutCheck);
}

function duGetFailure(o) {
	if (!($.cookie(o.argument[0]) == "true" && $.cookie("button_user") == dutUser)) {
		dutNotYet(o.argument,false);
	}
	receivedDutCheckResponse(o.argument, false);
}

function receivedDutCheckResponse(ele, isDutAlready) {
	dutCheckResults[ele[0]] = isDutAlready;
	if(++numberOfDutChecksCompleted === 4) {
		if(dutCheckResults["Phase2OpenAccess"]) {
			XNAT.app.HcpSplash.loadPhase2Data();	// this guy will close our modal when he's done
		}
		else {
			closeModalPanel("HcpSplashModal");
		}
	}	
}

var numberOfDutChecksCompleted;
var dutCheckResults;

function dutCheck() {

	// Check data use acceptance status
	var arg;
	var params;
	numberOfDutChecksCompleted = 0;
	dutCheckResults = {};

	openModalPanel("HcpSplashModal", "Loading data...");
		
	arg = ["WorkbenchData"];
	params = 'terms=WB&XNAT_CSRF='+window.csrfToken;
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/datause?'+params,{ success:duGetSuccess,failure:duGetFailure,argument:arg,cache:false });

	arg = ["Phase1PilotData"];
	params = 'terms=Phase1&XNAT_CSRF='+window.csrfToken;
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/datause?'+params,{ success:duGetSuccess,failure:duGetFailure,argument:arg,cache:false });

	arg = ["Phase2OpenAccess", "Phase2OpenAccess_GroupAverage"];
	params = 'terms=Phase2&XNAT_CSRF='+window.csrfToken;
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/datause?'+params,{ success:duGetSuccess,failure:duGetFailure,argument:arg,cache:false });

	arg = ["NKIRockland"];
	params = 'terms=NKI&XNAT_CSRF='+window.csrfToken;
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/datause?'+params,{ success:duGetSuccess,failure:duGetFailure,argument:arg,cache:false });
}

function dutAlready(eleArr) {
	for(var i = 0; i < eleArr.length; ++i) {
		var ele = eleArr[i];
		// DUT accepted - show agreed button
		$.cookie(ele,"true");
		$('#list-' + ele).find('.permission-controlled')
			.removeClass("hidden")
		
		$('#dut-processing-div').addClass("hidden").hide();
		$('#list-' + ele).find('input.request')
			.removeClass("hidden").show()
			.removeClass("request")
			.addClass("accepted")
			.attr("disabled","disabled")
			.val("Agreed")
		$('#div-' + ele).addClass("hidden").hide();
		$('#div2-' + ele).removeClass("hidden").show();
		$('#btn-' + ele).removeClass("hidden").show();
		
		// highlight acceptance of terms
		$('#list-' + ele + ' .dut')
			.addClass("accepted")
			.text("Terms of Use Accepted")
	}
}

var dutUser;

function dutNotYet(eleArr,force) {
	for(var i = 0; i < eleArr.length; ++i) {
		var ele = eleArr[i];
		// DUT not yet accepted - show request button
		if (force || !($.cookie(ele) == "true" && $.cookie("button_user") == dutUser)) {
			$.cookie(ele,"false");
		} 
		$('#dut-processing-div').addClass("hidden").hide();
		$('#list-' + ele).find('input.request')
			.removeClass("hidden")
		$('#div-' + ele).removeClass("hidden").show()
		$('#div2-' + ele).addClass("hidden").hide()
		$('#btn-' + ele).addClass("hidden").hide()
		
		// highlight acceptance of terms
		$('#list-' + ele + ' .dut')
			.removeClass("accepted")
			.text("Terms of Use")
	}
}

function initIndexWindow() {
	//alert("Vers 1");
	dutCheck();
}

function doInitButtons(user) {
	// Speed up setting up of buttons using values from cookies
	dutUser = $.cookie("button_user");
	if ( $.cookie("button_user") == user) {
		dutCookieSet("Phase1PilotData");
		dutCookieSet("Phase2OpenAccess");
		dutCookieSet("WorkbenchData");
	}
	$.cookie("button_user",user);
}

function dutCookieSet(ele) {
	if ($.cookie(ele) == "true" ) {
		dutAlready(ele);
	} else {
		dutNotYet(ele,false);
	}
}

function scrollFunc(ele,check) {
	if (ele.clientHeight + ele.scrollTop >= ele.scrollHeight) {
		$("#" + check).removeProp('disabled');
	}
}

function forgotGetSuccess(o) {
	$('#reg-processing-div').addClass("hidden").hide();
	$('#reg-thanks-div .modal-title').text("Forgot Username/Password Service");
	$('#reg-thanks-div').removeClass("hidden").show();
	$('#reg-thanks-msg').text(o.responseText);
}

function forgotGetFailure(o) {
	forgotGetSuccess(o);
}

function forgotLoginRequest() {
	params='account=' + $('#forgotPassword').val() + '&email=' + $('#forgotUsername').val() + '&XNAT_CSRF=' + window.csrfToken;
	$('#reg-processing-div').removeClass("hidden").show();
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/forgotlogin?'+params,{ success:forgotGetSuccess,failure:forgotGetFailure,cache:false });
}

/*
 * -- overlay function for data releases  --
 */
 
$(document).ready(function(){
	$('.show-more').hover(function(){
		// perform on hover
		$(this).addClass('hover');
	},function(){
		// perform on mouse-out
		$(this).removeClass('hover');
		
	});
});
