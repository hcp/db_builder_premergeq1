// Copyright 2010 Washington University School of Medicine All Rights Reserved
package org.nrg.xnat.restlet.resources.search;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.*;
import org.nrg.xdat.presentation.CSVPresenter;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.security.SecurityManager;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFT;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.MaterializedView;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXReader;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.FileUtils;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.restlet.extensions.DictionaryBasedSearch;
import org.nrg.xnat.restlet.presentation.RESTHTMLPresenter;
import org.nrg.xnat.restlet.representations.ItemXMLRepresentation;
import org.nrg.xnat.restlet.resources.ItemResource;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.FileRepresentation;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import org.springframework.util.StringUtils;
import org.xml.sax.SAXException;

import com.google.common.collect.Maps;
import com.noelios.restlet.ext.servlet.ServletCall;

public class DictSearchResource extends ItemResource {
    private final DataDictionaryService _service;
    private String categoryS;

    public DictSearchResource(Context context, Request request,
                              Response response) {
        super(context, request, response);

        //make sure we have access
        _service= XDAT.getContextService().getBean(DataDictionaryService.class);

        try {
            categoryS= URLDecoder.decode((String) getParameter(request, "CATEGORIES"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("",e);
        }

        if(_service!=null && categoryS!=null){
            this.getVariants().add(new Variant(MediaType.TEXT_XML));
        }
    }

    @Override
    public Representation getRepresentation(Variant variant) {
        MediaType mt = overrideVariant(variant);

        XdatStoredSearch xss=  createSearch(_service,categoryS,this.getQueryVariable("project"));


        if(xss!=null){
            if(filepath !=null && filepath.startsWith("results")){
                try {
                    DisplaySearch ds=xss.getDisplaySearch(user);
                    String sortBy = this.getQueryVariable("sortBy");
                    String sortOrder = this.getQueryVariable("sortOrder");
                    if (sortBy != null){
                        ds.setSortBy(sortBy);
                        if(sortOrder != null)
                        {
                            ds.setSortOrder(sortOrder);
                        }
                    }

                    MaterializedView mv=null;

                    if(xss.getId()!=null && !xss.getId().equals("")){
                        mv = MaterializedView.GetMaterializedViewBySearchID(xss.getId(), user);
                    }

                    if(mv!=null && (xss.getId().startsWith("@") || this.isQueryVariableTrue("refresh"))){
                        mv.delete();
                        mv=null;
                    }

                    LinkedHashMap<String,Map<String,String>> cp=SearchResource.setColumnProperties(ds,user,this);

                    XFTTable table=null;
                    if(mv!=null){
                        if (mt.equals(SecureResource.APPLICATION_XLIST)){
                            table=(XFTTable)ds.execute(new RESTHTMLPresenter(TurbineUtils.GetRelativePath(ServletCall.getRequest(this.getRequest())),this.getCurrentURI(),user,sortBy),user.getLogin());
                        }else if(mt!=null && (mt.equals(MediaType.APPLICATION_EXCEL) || mt.equals(SecureResource.TEXT_CSV)) && !this.isQueryVariableTrue("includeHiddenFields")){
                            table=(XFTTable)ds.execute(new CSVPresenter(),user.getLogin());

                            //copied from CSVScreen
                            java.util.Date today = java.util.Calendar.getInstance(java.util.TimeZone.getDefault()).getTime();
                            String fileName=user.getUsername() + "_" + (today.getMonth() + 1) + "_" + today.getDate() + "_" + (today.getYear() + 1900) + "_" + today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds() + ".csv";

                            this.setContentDisposition(fileName, false);
                        }
                        else{
                            table=mv.getData(null, null, null);
                        }
                    }else{
                        ds.setPagingOn(false);
                        if (mt.equals(SecureResource.APPLICATION_XLIST)){
                            table=(XFTTable)ds.execute(new RESTHTMLPresenter(TurbineUtils.GetRelativePath(ServletCall.getRequest(this.getRequest())),this.getCurrentURI(),user,sortBy),user.getLogin());
                        }else if(mt!=null && (mt.equals(MediaType.APPLICATION_EXCEL) || mt.equals(SecureResource.TEXT_CSV)) && !this.isQueryVariableTrue("includeHiddenFields")){
                            table=(XFTTable)ds.execute(new CSVPresenter(),user.getLogin());

                            //copied from CSVScreen
                            java.util.Date today = java.util.Calendar.getInstance(java.util.TimeZone.getDefault()).getTime();
                            String fileName=user.getUsername() + "_" + (today.getMonth() + 1) + "_" + today.getDate() + "_" + (today.getYear() + 1900) + "_" + today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds() + ".csv";

                            this.setContentDisposition(fileName, false);
                        }else{
                            table=(XFTTable)ds.execute(null,user.getLogin());
                        }

                    }

                    Hashtable<String,Object> tableParams=new Hashtable<String,Object>();
                    tableParams.put("totalRecords", table.getNumRows());

                    return this.representTable(table, mt, tableParams,cp);
                } catch (Exception e) {
                    e.printStackTrace();
                    this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
                }
            }else{
                if (mt.equals(MediaType.TEXT_XML)){
                    ItemXMLRepresentation rep= new ItemXMLRepresentation(xss.getItem(),MediaType.TEXT_XML);
                    rep.setAllowDBAccess(false);

                    return rep;
                }
            }
//	        else if (mt.equals(MediaType.APPLICATION_JSON)){
//				return new JSONTableRepresentation(item,params,MediaType.APPLICATION_JSON);
//			}else{
//				return new HTMLTableRepresentation(item,params,MediaType.TEXT_HTML);
//			}
        }

        return null;

    }

    @Override
    public boolean allowDelete() {
        return false;
    }

    @Override
    public boolean allowPut() {
        return false;
    }

    public XdatStoredSearch createSearch(DataDictionaryService _service, String categoryS, String project){
        String[] categories={};

        if(org.apache.commons.lang.StringUtils.isNotEmpty(categoryS)){
            categories=categoryS.split(",");
        }

        //initialize search
        final XdatStoredSearch search = new XdatStoredSearch((UserI)user);
        search.setRootElementName("xnat:subjectData");
        search.setDescription(categoryS);
        search.setBriefDescription(categoryS);

        int count=0;

        //add all fields from all columns visible=false unless the categories matche requested ones
        for(final String category:_service.getCategories()){
            for(final String assessment:_service.getAssessments(category)){
                for(final Attribute attribute:_service.getAttributes(category, assessment)){
                    final XdatSearchField column=new XdatSearchField((UserI)user);
                    column.setElementName(attribute.getXsiType());
                    column.setFieldId(attribute.getFieldId());
                    column.setHeader(attribute.getLabel());
                    column.setSequence(count++);
                    if(!ArrayUtils.contains(categories, category) && !attribute.getSystemId().equals("xnat:subjectData/subject_label")){
                        column.setVisible("false");
                    }
                    try{
                        search.setSearchField(column);
                    }
                    catch(Exception e){
                          logger.error(""+e);
                    }
                }
            }
        }
        try{
            //add MR Columns, if requested.  This is temporarily hard coded because the data dictionary doesn't currently support unfilterable columns yet.
            if(ArrayUtils.contains(categories, "MR Sessions")){
                String[][] mr_fields={
                        {"LABEL","Label"},
                        {"SCANNER","Scanner"},
                        {"MR_SCAN_COUNT_AGG","Scans"}
                };

                for(String[] field:mr_fields){
                    final XdatSearchField column=new XdatSearchField((UserI)user);
                    column.setElementName("xnat:mrSessionData");
                    column.setFieldId(field[0]);
                    column.setHeader(field[1]);
                    column.setSequence(count++);

                    search.setSearchField(column);
                }

                search.setRootElementName("xnat:mrSessionData");
            }

            //add constraints for project if requested
            if(project!=null){
                final XdatCriteriaSet cs= new XdatCriteriaSet((UserI)user);
                cs.setMethod("OR");

                for(final String p: org.springframework.util.StringUtils.commaDelimitedListToSet(project)){
                    XdatCriteria c=new XdatCriteria((UserI)user);
                    c.setSchemaField(search.getRootElementName()+"/project");
                    c.setComparisonType("=");
                    c.setValue(p);
                    cs.setCriteria(c);

                    c=new XdatCriteria((UserI)user);
                    c.setSchemaField(search.getRootElementName()+"/sharing/share/project");
                    c.setComparisonType("=");
                    c.setValue(p);
                    cs.setCriteria(c);
                }

                search.setSearchWhere(cs);
            }
        }
        catch(Exception e){
            logger.error(""+e);
        }
        return search;
    }
}
