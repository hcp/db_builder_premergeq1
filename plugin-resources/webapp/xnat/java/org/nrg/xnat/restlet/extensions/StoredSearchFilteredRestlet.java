//Copyright 2012 Radiologics, Inc
//Author: Tim Olsen <tim@radiologics.com>
package org.nrg.xnat.restlet.extensions;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.turbine.util.TurbineException;
import org.nrg.action.ActionException;
import org.nrg.xdat.presentation.CSVPresenter;
import org.nrg.xdat.presentation.PresentationA;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.security.XdatStoredSearch;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.PoolDBUtils;
import org.nrg.xft.utils.StringUtils;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.presentation.RESTHTMLPresenter;
import org.nrg.xnat.restlet.representations.StandardTurbineScreen;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.resources.search.CachedSearchResource;
import org.nrg.xnat.restlet.resources.search.MaterializedViewForFilter;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.Variant;

import com.google.common.collect.Maps;
import com.noelios.restlet.ext.servlet.ServletCall;

/**
 * @author tim@radiologics.com
 *
 * Restlet used to filter and group cached search results.
 */
@XnatRestlet("/services/search/filter/{CACHED_SEARCH_ID}")
public class StoredSearchFilteredRestlet  extends SecureResource {
	static org.apache.log4j.Logger logger = Logger.getLogger(CachedSearchResource.class);
	String tableName=null;
	String columnName=null;
	
	Integer offset=null;
	Integer rowsPerPage=null;
	String sortBy=null;
	String sortOrder="ASC";
	
	
	/**
	 * @param context standard
	 * @param request standard
	 * @param response standard
	 */
	public StoredSearchFilteredRestlet(Context context, Request request, Response response) {
		super(context, request, response);
			
		tableName=(String)getParameter(request,"CACHED_SEARCH_ID");
		
		if(PoolDBUtils.HackCheck(tableName)){
		    AdminUtils.sendAdminEmail(user,"Possible SQL Injection Attempt", "SORT BY:" + sortOrder);
			response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
			return;
		}
		
		columnName=(String)getQueryVariable("columns");
		
		if (this.getQueryVariable("offset")!=null){
			try {
				offset=Integer.valueOf(this.getQueryVariable("offset"));
			} catch (NumberFormatException e) {
				response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
				return;
			}
		}
		
		if (this.getQueryVariable("limit")!=null){
			try {
				rowsPerPage=Integer.valueOf(this.getQueryVariable("limit"));
			} catch (NumberFormatException e) {
				response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
				return;
			}
		}
		
		if (this.getQueryVariable("sortBy")!=null){
			sortBy=this.getQueryVariable("sortBy");
			if(PoolDBUtils.HackCheck(sortBy)){
		     AdminUtils.sendAdminEmail(user,"Possible SQL Injection Attempt", "SORT BY:" + sortOrder);
				response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
				return;
			}
			sortBy=StringUtils.ReplaceStr(sortBy, " ", "");
		}
		
		if (this.getQueryVariable("sortOrder")!=null){
			sortOrder=this.getQueryVariable("sortOrder");
			if(PoolDBUtils.HackCheck(sortOrder)){
		     AdminUtils.sendAdminEmail(user,"Possible SQL Injection Attempt", "SORT ORDER:" + sortOrder);
				response.setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY);
				return;
			}
			sortOrder=StringUtils.ReplaceStr(sortOrder, " ", "");
		}
					
		this.getVariants().add(new Variant(MediaType.APPLICATION_JSON));
		this.getVariants().add(new Variant(MediaType.TEXT_HTML));
		this.getVariants().add(new Variant(MediaType.TEXT_XML));
	}

	/**
	 * @param table name
	 * @param user object
	 * @return materialized view with that table name
	 * @throws Exception from database interactoin
	 */
	private MaterializedViewForFilter getView(String table, XDATUser user) throws Exception{
		MaterializedViewForFilter mv=MaterializedViewForFilter.GetMaterializedView(tableName, user);
		if(mv!=null){
			return mv;
		}
		
		mv=MaterializedViewForFilter.GetMaterializedViewBySearchID(tableName, user);
		
		if(mv!=null){
			return mv;
		}
		
		if(tableName.startsWith("@")){
			//special handling
			//build default listing
			String dv = "listing";
			DisplaySearch ds = new DisplaySearch();
			ds.setUser(user);
			ds.setDisplay(dv);
			ds.setRootElement(tableName.substring(1));
			XdatStoredSearch search=ds.convertToStoredSearch(tableName);
			search.setId(tableName);
			
			ds.setPagingOn(false);
			
			String query = ds.getSQLQuery(null);
			query = StringUtils.ReplaceStr(query,"'","*'*");
			query = StringUtils.ReplaceStr(query,"*'*","''");
			
			mv = new MaterializedViewForFilter(user);
			if(search.getId()!=null && !search.getId().equals("")){
				mv.setSearch_id(search.getId());
			}
			mv.setSearch_sql(query);
			mv.setSearch_xml(search.getItem().writeToFlatString(0));
			mv.save();
			
			return mv;
		}else{
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource.Variant)
	 */
	@Override
	public Representation getRepresentation(Variant variant) {
		MediaType mt = overrideVariant(variant);
		
		Hashtable<String,Object> params=new Hashtable<String,Object>();
		if(tableName!=null){
			params.put("ID", tableName);
		}
		if(columnName!=null){
			params.put("columns", columnName);
		}
		
		XFTTable table=null;
		
		try {
			MaterializedViewForFilter mv =getView(tableName, user);
			if(mv.getUser_name().equals(user.getLogin())){
				if(columnName!=null){
					MaterializedViewForFilter.validateColumns(columnName,mv);
					
					table=mv.getColumnsValues(columnName,buildWhere(mv));
				}else{
					table=mv.getData((sortBy!=null)?sortBy + " " + sortOrder:null, offset, rowsPerPage, buildWhere(mv));
					

					params.put("totalRecords", mv.getSize(buildWhere(mv)));
					
					if (mt!=null && (mt.equals(SecureResource.APPLICATION_XLIST))){
						table=formatTable((new RESTHTMLPresenter(TurbineUtils.GetRelativePath(ServletCall.getRequest(this.getRequest())),null,user,sortBy)),mv,table, user);
				    }else if (mt!=null && (mt.equals(MediaType.APPLICATION_EXCEL) || mt.equals(SecureResource.TEXT_CSV)) && !this.isQueryVariableTrue("includeHiddenFields")){
						table=formatTable(new CSVPresenter(),mv,table, user);
						
						//copied from CSVScreen
						java.util.Date today = java.util.Calendar.getInstance(java.util.TimeZone.getDefault()).getTime();
						String fileName=user.getUsername() + "_" + (today.getMonth() + 1) + "_" + today.getDate() + "_" + (today.getYear() + 1900) + "_" + today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds() + ".csv";
						
						this.setContentDisposition(fileName, false);
				    }
				}
			}
		} catch (ActionException e) {
			this.getResponse().setStatus(e.getStatus());
			table = new XFTTable();
		} catch (SQLException e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.CLIENT_ERROR_GONE);
			table = new XFTTable();
		} catch (Exception e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			table = new XFTTable();
		}

		
		try {
			if(hasQueryVariable("requested_screen")){
				String tag="t_"+Calendar.getInstance().getTimeInMillis();
				this.getHttpSession().setAttribute(tag, table);
				params.put("hideTopBar",isQueryVariableTrue("hideTopBar"));
				params.put("table_tag", tag);
				return new StandardTurbineScreen(MediaType.TEXT_HTML, getRequest(), user, getQueryVariable("requested_screen"), params);
			}else{
				return representTable(table,mt,params);
			}
		} catch (TurbineException e) {
			logger.error("",e);
			this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			return null;
		}
	}
	
	private static XFTTable formatTable(PresentationA presenter, MaterializedViewForFilter mv, XFTTable table, XDATUser user) throws Exception{
		DisplaySearch ds = mv.getDisplaySearch(user);
		ds.getSQLQuery(presenter);
    	
    	presenter.setRootElement(ds.getRootElement());
		presenter.setDisplay(ds.getDisplay());
		presenter.setAdditionalViews(ds.getAdditionalViews());
		return (XFTTable)presenter.formatTable(table,ds,false);
	}
	
	/**
	 * @param mv materialized view
	 * @return map representing the where clause
	 * @throws Exception from MaterializedViewForFilter.getColumnNames()
	 */
	public Map<String,Object> buildWhere(MaterializedViewForFilter mv) throws Exception{
		Map<String,Object> map=Maps.newHashMap();
		
		List<String> columns=mv.getColumnNames();
		
		for(Map.Entry<String,Object> entry:this.getQueryVariablesAsMap().entrySet()){
			if(columns.contains(entry.getKey())){
				map.put(entry.getKey(), entry.getValue());
			}
		}
		
		for(Map.Entry<String,String> entry:this.getBodyVariableMap().entrySet()){
			if(columns.contains(entry.getKey())){
				map.put(entry.getKey(), entry.getValue());
			}
		}
		
		return map;
	}
}
