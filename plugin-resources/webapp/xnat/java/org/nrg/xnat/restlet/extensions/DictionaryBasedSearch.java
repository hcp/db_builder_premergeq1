package org.nrg.xnat.restlet.extensions;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.bean.XdatCriteriaBean;
import org.nrg.xdat.bean.XdatCriteriaSetBean;
import org.nrg.xdat.bean.XdatSearchBean;
import org.nrg.xdat.bean.XdatSearchFieldBean;
import org.nrg.xdat.bean.XdatStoredSearchBean;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.representations.BeanRepresentation;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.resource.Representation;
import org.restlet.resource.Variant;

/**
 * @author tim@deck5consulting.com
 * @since 2/22/13
 *
 * Restlet used to build search xmls based on the data dictionary contents
 */
@XnatRestlet("/services/search/ddict/{CATEGORIES}")
public class DictionaryBasedSearch  extends SecureResource {
	static org.apache.log4j.Logger logger = Logger.getLogger(DictionaryBasedSearch.class);
	private final DataDictionaryService _service;
	private String categoryS;
	/**
	 * @param context standard
	 * @param request standard
	 * @param response standard
	 */
	public DictionaryBasedSearch(Context context, Request request, Response response) {
		super(context, request, response);
		
		//make sure we have access
		_service= XDAT.getContextService().getBean(DataDictionaryService.class);
		
		try {
			categoryS=URLDecoder.decode((String)getParameter(request, "CATEGORIES"),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("",e);
		}
		
		if(_service!=null && categoryS!=null){
			this.getVariants().add(new Variant(MediaType.TEXT_XML));
		}
	}
	
	/* (non-Javadoc)
	 * @see org.restlet.resource.Resource#getRepresentation(org.restlet.resource.Variant)
	 */
	@Override
	public Representation getRepresentation(Variant variant) {
		final MediaType mt = overrideVariant(variant);
		
		String[] categories={};
		
		if(StringUtils.isNotEmpty(categoryS)){
			categories=categoryS.split(",");
		}
		
		//initialize search
		final XdatStoredSearchBean search = new XdatStoredSearchBean();
		search.setRootElementName("xnat:subjectData");
		search.setDescription(categoryS);
		search.setBriefDescription(categoryS);
		
		int count=0;
		
		//add all fields from all columns visible=false unless the categories matche requested ones
		for(final String category:_service.getCategories()){
			for(final String assessment:_service.getAssessments(category)){
				for(final Attribute attribute:_service.getAttributes(category, assessment)){
					final XdatSearchFieldBean column=new XdatSearchFieldBean();
					column.setElementName(attribute.getXsiType());
					column.setFieldId(attribute.getFieldId());
					column.setHeader(attribute.getLabel());
					column.setSequence(count++);
					if(!ArrayUtils.contains(categories, category) && !attribute.getSystemId().equals("xnat:subjectData/subject_label")){
						column.setVisible("false");
					}
					search.addSearchField(column);
				}
			}
		}
		 
		//add MR Columns, if requested.  This is temporarily hard coded because the data dictionary doesn't currently support unfilterable columns yet.
		if(ArrayUtils.contains(categories, "MR Sessions")){
			String[][] mr_fields={
					{"LABEL","Label"},
					{"SCANNER","Scanner"},
					{"MR_SCAN_COUNT_AGG","Scans"}
			};
			
			for(String[] field:mr_fields){
				final XdatSearchFieldBean column=new XdatSearchFieldBean();
				column.setElementName("xnat:mrSessionData");
				column.setFieldId(field[0]);
				column.setHeader(field[1]);
				column.setSequence(count++);
				search.addSearchField(column);
			}
			
			search.setRootElementName("xnat:mrSessionData");
		}
		
		//add constraints for project if requested
		if(this.getQueryVariable("project")!=null){
			final XdatCriteriaSetBean cs= new XdatCriteriaSetBean();
			cs.setMethod("OR");
			
			for(final String p: org.springframework.util.StringUtils.commaDelimitedListToSet(this.getQueryVariable("project"))){
				XdatCriteriaBean c=new XdatCriteriaBean();
				c.setSchemaField(search.getRootElementName()+"/project");
				c.setComparisonType("=");
				c.setValue(p);
				cs.addCriteria(c);

				c=new XdatCriteriaBean();
				c.setSchemaField(search.getRootElementName()+"/sharing/share/project");
				c.setComparisonType("=");
				c.setValue(p);
				cs.addCriteria(c);
			}
			
			search.addSearchWhere(cs);
		}
		
		return new BeanRepresentation(search,mt);
	}
}
