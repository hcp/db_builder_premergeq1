/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.xnat.exceptions;


/**
 * @author Timothy R. Olsen <olsent@wustl.edu>
 *
 */
public class ValidationException extends Exception {
	public ValidationException(String s){
		super(s);
	}
}
