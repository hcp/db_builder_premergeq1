/*
 * ArchiveMetaData
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * ArchiveMetaData
 *
 * @author rherri01
 * @since 2/15/13
 */
public class ArchiveMetaData {
    /**
     * Returns a file system object for a Zip archive.
     * @param filename     The full path to the Zip file
     * @param create       <b>true</b> if the Zip file should be created
     * @return A Zip file system
     * @throws IOException
     */
    public static FileSystem createZipFileSystem(String filename, boolean create) throws IOException {
        // Convert filename to URI
        final Path path = Paths.get(filename);
        final URI uri = URI.create("jar:file:" + path.toUri().getPath());

        final Map<String, String> env = new HashMap<String, String>();
        if (create) {
            env.put("create", "true");
        }
        return FileSystems.newFileSystem(uri, env);
    }

    /**
     * Returns the contents of the specified Zip file as a map containing path information.
     * @param filename    The full path to the Zip file
     * @throws java.io.IOException
     */
    public static Map<EntryType, Set<Path>> getArchiveMap(String filename) throws IOException{

        if (_log.isDebugEnabled()) {
            _log.debug(String.format("Mapping archive:  %s\n", filename));
        }

        final Set<Path> files = new HashSet<Path>();
        final Set<Path> folders = new HashSet<Path>();

        FileSystem fileSystem = null;

        try {
            fileSystem = createZipFileSystem(filename, false);
            final Path root = fileSystem.getPath("/");

            // Walk the file tree and print out the directory and file names
            Files.walkFileTree(root, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
                    if (_log.isDebugEnabled()) {
                        log(file);
                    }
                    files.add(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult preVisitDirectory(Path folder, BasicFileAttributes attributes) throws IOException {
                    if (_log.isDebugEnabled()) {
                        log(folder);
                    }
                    folders.add(folder);
                    return FileVisitResult.CONTINUE;
                }

                /**
                 * Logs details about the specified path such as size and modification time
                 * @param file    The file path to be logged.
                 * @throws java.io.IOException
                 */
                private void log(Path file) throws IOException {
                    _log.debug(String.format("%d  %s  %s\n", Files.size(file), DATE_FORMAT.format(new Date( Files.getLastModifiedTime(file).toMillis())), file));
                }
            });
        } finally {
            if (fileSystem != null) {
                fileSystem.close();
            }
        }

        return new HashMap<EntryType, Set<Path>>() {{
            put(EntryType.File, files);
            put(EntryType.Folder, folders);
        }};
    }

    public enum EntryType {
        File,
        Folder
    }

    private static final Log _log = LogFactory.getLog(ArchiveMetaData.class);
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy-HH:mm:ss");
}
