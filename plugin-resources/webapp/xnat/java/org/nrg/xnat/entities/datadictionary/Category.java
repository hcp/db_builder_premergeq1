/*
 * Category
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.entities.datadictionary;

/**
 * Represents a category.
 *
 * @author rherri01
 * @since 2/18/13
 */
public class Category extends Node {
    public Category() {
        super();
    }

    public Category(final int position, final String name, final String briefName, final String label, final String description) {
        super(position, name, briefName, label, description);
    }

    private static final long serialVersionUID = 4351354641917131046L;
}
