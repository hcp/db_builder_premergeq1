/*
 * Node
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.entities.datadictionary;

import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Comparator;

/**
 * This class is the most atomic representation of an entity in the XNAT data dictionary.
 *
 * @author rherri01
 * @since 2/18/13
 */
@XmlRootElement
public class Node implements Serializable {

    public Node() {
        // Default constructor
    }

    public Node(final int position, final String name, final String briefName, final String label, final String description) {
        setPosition(position);
        setName(name);
        setBriefName(briefName);
        setLabel(label);
        setDescription(description);
    }

    @XmlID
    public String getSystemId() {
        return getClass().getName() + "/" + _name;
    }

    /**
     * TODO: Position is a <i>total hack</i>. Replace this with linked nodes: siblings, parents, and children.
     * @return This node's relative position in relation to its siblings.
     */
    public int getPosition() {
        return _position;
    }

    public void setPosition(int position) {
        _position = position;
    }

    public String getName() {
        return _name;
    }

    public void setName(final String name) {
        _name = name;
    }

    public String getBriefName() {
        return _briefName;
    }

    public void setBriefName(final String briefName) {
        _briefName = briefName;
    }

    public String getLabel() {
        return _label;
    }

    public void setLabel(final String label) {
        _label = label;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    /**
     * Used for sorting of nodes based on the position property.
     *
     * @author rherri01
     * @since 2/19/13
     */
    public static class NodeComparator implements Comparator<Node> {
        @Override
        public int compare(final Node node1, final Node node2) {
            return node1.getPosition() == node2.getPosition() ? 0 : (node1.getPosition() < node2.getPosition() ? -1 : 1);
        }
    }

    private static final long serialVersionUID = 3074427465020840486L;

    private int _position;
    private String _name;
    private String _briefName;
    private String _label;
    private String _description;
}
