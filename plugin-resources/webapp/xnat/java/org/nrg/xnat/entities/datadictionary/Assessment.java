/*
 * Assessment
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.entities.datadictionary;

/**
 * Assessment
 *
 * @author rherri01
 * @since 2/18/13
 */
public class Assessment extends Node {
    public Assessment() {
        super();
    }

    public Assessment(final int position, final String name, final String category, final String briefName, final String label, final String description) {
        super(position, name, briefName, label, description);
        setCategory(category);
    }

    public String getCategory() {
        return _category;
    }

    public void setCategory(final String category) {
        _category = category;
    }

    private static final long serialVersionUID = -6450976298430095961L;

    private String _category;
}
