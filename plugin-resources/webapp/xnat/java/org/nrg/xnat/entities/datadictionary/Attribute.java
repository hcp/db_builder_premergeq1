/*
 * Attribute
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.entities.datadictionary;

import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

/**
 * Attribute
 *
 * @author rherri01
 * @since 1/24/13
 */
@XmlRootElement
public class Attribute extends Node {

    @Override
    @XmlID
    public String getSystemId() {
        return _xsiType + "/" + getName();
    }

    public String getXsiType() {
        return _xsiType;
    }

    public void setXsiType(final String xsiType) {
        _xsiType = xsiType;
    }

    public String getCategory() {
        return _category;
    }

    public void setCategory(final String category) {
        _category = category;
    }

    public String getAssessment() {
        return _assessment;
    }

    public void setAssessment(final String assessment) {
        _assessment = assessment;
    }

    public String getFieldId() {
        return _fieldId;
    }

    public void setFieldId(final String fieldId) {
    	_fieldId = fieldId;
    }

    public Map<String, String> getOperators() {
        return _operators;
    }

    public void setOperators(final Map<String, String> operators) {
        _operators = operators;
    }

    public Map<String, String> getValues() {
        return _values;
    }

    public void setValues(final Map<String, String> values) {
        _values = values;
    }

    /**
     * Indicates whether multiple instances of this attribute in a query or filter should be AND'ed or OR'ed together by
     * default. Note that either setting for this doesn't imply that you can't perform the opposite comparison.
     * @return <b>true</b> if this attribute should be combined as ANDs, <b>false</b> otherwise.
     */
    public boolean isCombinedAnd() {
        return _isCombinedAnd;
    }

    public void setCombinedAnd(final boolean combinedAnd) {
        _isCombinedAnd = combinedAnd;
    }

    public String getValidation() {
        return _validation;
    }

    public void setValidation(final String validation) {
        _validation = validation;
    }

    public String getValidationMessage() {
        return _validationMessage;
    }

    public void setValidationMessage(final String validationMessage) {
        _validationMessage = validationMessage;
    }
    
    public String getWatermark() {
        return _watermark;
    }

    public void setWatermark(String watermark) {
        this._watermark = watermark;
    }

    public String getDictName() {
        return _dictName;
    }

    public void setDictName(String dictName) {
        this._dictName = dictName;
    }

    @Override
    public String toString() {
	return ToStringBuilder.reflectionToString(this);
    }
    
    private String _xsiType;
    private String _category;
    private String _assessment;
    private String _fieldId;
    private Map<String, String> _operators;
    private Map<String, String> _values;
    private boolean _isCombinedAnd;
    private String _validation;
    private String _validationMessage;
    private String _watermark;
    private String _dictName;
}
