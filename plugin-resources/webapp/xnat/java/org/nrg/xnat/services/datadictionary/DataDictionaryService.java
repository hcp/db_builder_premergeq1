/*
 * DataDictionaryService
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.services.datadictionary;

import org.codehaus.jackson.JsonGenerationException;
import org.nrg.framework.services.NrgService;
import org.nrg.xnat.entities.datadictionary.Attribute;

import java.io.IOException;
import java.util.List;

/**
 * DataDictionaryService
 *
 * @author rherri01
 * @since 1/24/13
 */
public interface DataDictionaryService extends NrgService {
    public static String SERVICE_NAME = "DataDictionaryService";

    /**
     * Gets a list of all attributes available for querying.
     * @return A list of the available attributes.
     */
    public List<Attribute> getAttributes();
    /**
     * Gets a list of all attributes available for querying from the indicated data type.
     * @param dataType    The data type for which you want to retrieve attributes.
     * @return A list of the available attributes for the indicated data type.
     */
    public List<Attribute> getAttributes(final String dataType);

    /**
     * Gets the indicated attribute from the indicated data type
     * @param dataType    The data type for which you want to retrieve attributes.
     * @param attribute   The particular attribute you want to retrieve..
     * @return The indicated attribute.
     */
    public Attribute getAttribute(final String dataType, final String attribute);

    /**
     * Gets the indicated attribute from the indicated data type
     * @param dataType    The data type for which you want to retrieve attributes.
     * @param fieldId   The particular attribute you want to retrieve..
     * @return The indicated attribute.
     */
    public Attribute getAttributeByFieldId(final String dataType, final String fieldId);

    /**
     * Gets all of the available categories.
     * @return A list of strings with the category names.
     */
    public List<String> getCategories();

    /**
     * Gets a list of all assessments in the specified category
     * @param category      The category for which you want to retrieve assessments.
     * @return A list of the available assessments in the specified category.
     */
    public List<String> getAssessments(final String category);

    /**
     * Gets a list of all attributes in the specified assessment
     * @param category      The category for which you want to retrieve attributes.
     * @param assessment    The assessment for which you want to retrieve attributes.
     * @return A list of the available attributes in the specified assessment.
     */
    public List<Attribute> getAttributes(final String category, final String assessment);

    /**
     * Gets the indicated attribute.
     * @param category    The category for which you want to retrieve attributes.
     * @param assessment  The assessment for which you want to retrieve attributes.
     * @param attribute   The particular attribute you want to retrieve.
     * @return The indicated attribute.
     */
    public Attribute getAttribute(final String category, final String assessment, final String attribute);

    /**
     * Validates the submitted value for the given attribute.
     * @param dataType    The data type for which you want to validate an attribute.
     * @param attribute   The particular attribute for which you want to validate a value.
     * @param value       The value you want to validate.
     * @return A JSON payload indicating the outcome of the validation operation. The primary item in the resulting map
     * is "pass", which will be either true or false. Optional items may include messages and other validation
     * assistance.
     */
    public String validate(final String dataType, final String attribute, final String value) throws IOException;

    /**
     * Validates the submitted value for the given attribute.
     * @param category    The category for the attribute against which you want to validate
     * @param assessment  The assessment for the attribute against which you want to validate
     * @param attribute   The particular attribute for which you want to validate a value.
     * @param value       The value you want to validate.
     * @return A JSON payload indicating the outcome of the validation operation. The primary item in the resulting map
     * is "pass", which will be either true or false. Optional items may include messages and other validation
     * assistance.
     */
    public String validate(final String category, final String assessment, final String attribute, final String value) throws IOException;

    /**
     * Validates the attribute against the given value.
     * @param attribute    The attribute to be validated.
     * @param value        The value to validate.
     * @return A JSON payload indicating the outcome of the validation operation. The primary item in the resulting map
     *         is "pass", which will be either true or false. Optional items may include messages and other validation
     *         assistance.
     * @throws IOException
     */
    public String validate(final Attribute attribute, final String value) throws IOException;

    /**
     * A standard helper function to transform all of the attributes in the service into usable JSON.
     * @return The JSON representing the attributes.
     */
    public String toJson() throws IOException;

    /**
     * A standard helper function to transform a list of attributes into usable JSON.
     * @param attributes    The attributes to be transformed.
     * @return The JSON representing the attributes.
     */
    public String toJson(final List<Attribute> attributes) throws IOException;

    /**
     * A standard helper function to transform an attribute into usable JSON.
     * @param attribute    The attribute to be transformed.
     * @return The JSON representing the attribute.
     */
    public String toJson(final Attribute attribute) throws IOException;

    /**
     * Gets a list of all categories that these attributes are found in
     * @return A list of the categories.
     */
    public String getCategoriesString();

    /**
     * Gets a list of all assessments in the specified category
     * @param category    The category for which you want to retrieve assessments.
     * @return A list of the categories.
     */
    public String getAssessmentsString(final String category);

    /**
     * Gets a JSON-formatted list of the data dictionary's categories.
     * @return A JSON-formatted list of the data dictionary's categories.
     */
    public String getCategoryMap() throws IOException;

    /**
     * Gets a JSON-formatted map of the data dictionary's assessments, keyed to the assessments' containing categories.
     * @return A JSON-formatted map of the data dictionary's assessments.
     */
    public String getAssessmentMap() throws IOException;

    /**
     * Gets the full attributes map as JSON.
     *
     * @return A map of all the attributes as JSON.
     */
    public String getAttributeMap() throws IOException;
}
