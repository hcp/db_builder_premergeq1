/*
 * SimpleDataDictionaryService
 * Copyright (c) 2013. Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD License
 */

package org.nrg.xnat.services.datadictionary.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.TypeReference;
import org.nrg.xnat.entities.datadictionary.Assessment;
import org.nrg.xnat.entities.datadictionary.Attribute;
import org.nrg.xnat.entities.datadictionary.Category;
import org.nrg.xnat.entities.datadictionary.Node;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.*;

/**
 * SimpleDataDictionaryService
 * This simple service takes JSON to initialize. This is good for creating stable, static, or experimental data
 * dictionaries.
 *
 * @author rherri01
 * @since 1/24/13
 */
@SuppressWarnings({"SpringJavaAutowiringInspection", "MismatchedQueryAndUpdateOfCollection"})
@Service
public class SimpleDataDictionaryService implements DataDictionaryService, InitializingBean {
    public SimpleDataDictionaryService() throws IOException {
        _log.debug("Created default simple dictionary service");
    }

    public SimpleDataDictionaryService(final String categories, final String assessments, final String attributes) throws IOException {
        _log.debug("Created simple dictionary service from JSON");
        initializeCategoriesFromJson(categories);
        initializeAssessmentsFromJson(assessments);
        initializeAttributesFromJson(attributes);
    }

    /**
     * Gets a list of all attributes available for querying.
     *
     * @return A list of the available attributes.
     */
    @Override
    public List<Attribute> getAttributes() {
        return new ArrayList<Attribute>(_attributes.values());
    }

    /**
     * Gets a list of all attributes available for querying from the
     *
     * @return A list of the available attributes.
     */
    @Override
    public List<Attribute> getAttributes(final String dataType) {
        if (!_attributesByXsiTypeMap.containsKey(dataType)) {
            return null;
        }
        return new ArrayList<Attribute>(_attributesByXsiTypeMap.get(dataType).values());
    }

    /**
     * Gets the indicated attribute from the indicated data type
     *
     * @param dataType  The data type for which you want to retrieve attributes.
     * @param attribute The particular attribute you want to retrieve..
     * @return The indicated attribute.
     */
    @Override
    public Attribute getAttribute(final String dataType, final String attribute) {
        if (!_attributesByXsiTypeMap.containsKey(dataType)) {
            return null;
        }
        if (!_attributesByXsiTypeMap.get(dataType).containsKey(attribute)) {
            return null;
        }
        return _attributesByXsiTypeMap.get(dataType).get(attribute);
    }
    
    /**
     * Gets the indicated attribute from the indicated data type
     * @param dataType    The data type for which you want to retrieve attributes.
     * @param fieldId   The particular attribute you want to retrieve..
     * @return The indicated attribute.
     */
    @Override
	public Attribute getAttributeByFieldId(String dataType, String fieldId) {
    	if (!_attributesByXsiTypeFieldMap.containsKey(dataType)) {
            return null;
        }
        if (!_attributesByXsiTypeFieldMap.get(dataType).containsKey(fieldId)) {
            return null;
        }
        return _attributesByXsiTypeFieldMap.get(dataType).get(fieldId);
	}

    /**
     * Gets all of the available categories.
     *
     * @return A list of strings with the category names.
     */
    @Override
    public List<String> getCategories() {
        return _categoryNames;
    }

    /**
     * Gets a list of all assessments in the specified category
     *
     * @param category The category for which you want to retrieve assessments.
     * @return A list of the available assessments in the specified category.
     */
    @Override
    public List<String> getAssessments(final String category) {
        return _assessmentNames.get(category);
    }

    /**
     * Gets a list of all attributes in the specified assessment
     *
     * @param category   The category for which you want to retrieve attributes.
     * @param assessment The assessment for which you want to retrieve attributes.
     * @return A list of the available attributes in the specified assessment.
     */
    @Override
    public List<Attribute> getAttributes(final String category, final String assessment) {
        if (!_attributeMap.containsKey(category)) {
            return null;
        }
        if (!_attributeMap.get(category).containsKey(assessment)) {
            return null;
        }
        return new ArrayList<Attribute>(_attributeMap.get(category).get(assessment).values());
    }

    /**
     * Gets the indicated attribute.
     *
     * @param category   The category for which you want to retrieve attributes.
     * @param assessment The assessment for which you want to retrieve attributes.
     * @param attribute  The particular attribute you want to retrieve.
     * @return The indicated attribute.
     */
    @Override
    public Attribute getAttribute(final String category, final String assessment, final String attribute) {
        if (!_attributeMap.containsKey(category)) {
            return null;
        }
        if (!_attributeMap.get(category).containsKey(assessment)) {
            return null;
        }
        for (Attribute candidate : _attributeMap.get(category).get(assessment).values()) {
            if (candidate.getName().equals(attribute)) {
                return candidate;
            }
        }
            return null;
        }

    /**
     * Validates the submitted value for the given attribute.
     *
     * @param dataType  The data type for which you want to validate an attribute.
     * @param attribute The particular attribute for which you want to validate a value.
     * @param value     The value you want to validate.
     * @return A JSON payload indicating the outcome of the validation operation. The primary item in the resulting map
     *         is "pass", which will be either true or false. Optional items may include messages and other validation
     *         assistance.
     */
    @Override
    public String validate(final String dataType, final String attribute, final String value) throws IOException {
        final Attribute attribute1 = getAttribute(dataType, attribute);
        if (attribute1 == null) {
            throw new RuntimeException(String.format("The specified attribute doesn't exist to be validated: [%s/%s]", dataType, attribute));
        }
        return validate(attribute1, value);
    }

    /**
     * Validates the submitted value for the given attribute.
     *
     * @param category   The category for the attribute against which you want to validate
     * @param assessment The assessment for the attribute against which you want to validate
     * @param attribute  The particular attribute for which you want to validate a value.
     * @param value      The value you want to validate.
     * @return A JSON payload indicating the outcome of the validation operation. The primary item in the resulting map
     *         is "pass", which will be either true or false. Optional items may include messages and other validation
     *         assistance.
     */
    @Override
    public String validate(final String category, final String assessment, final String attribute, final String value) throws IOException {
        final Attribute attribute1 = getAttribute(category, assessment, attribute);
        if (attribute1 == null) {
            throw new RuntimeException(String.format("The specified attribute doesn't exist to be validated: [%s:%s:%s]", category, assessment, attribute));
        }
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Validates the attribute against the given value.
     *
     * @param attribute The attribute to be validated.
     * @param value     The value to validate.
     * @return A JSON payload indicating the outcome of the validation operation. The primary item in the resulting map
     *         is "pass", which will be either true or false. Optional items may include messages and other validation
     *         assistance.
     * @throws IOException
     */
    @Override
    public String validate(final Attribute attribute, final String value) throws IOException {
        String validation = attribute.getValidation();
        Map<String, Object> results = new Hashtable<String, Object>();
        results.put("pass", value != null ? value.matches(validation) : "".matches(validation));
        return _mapper.writeValueAsString(results);
    }

    /**
     * A standard helper function to transform all of the attributes in the service into usable JSON.
     *
     * @return The JSON representing the attributes.
     */
    @Override
    public String toJson() throws IOException {
        return toJson(new ArrayList<Attribute>(_attributes.values()));
    }

    /**
     * A standard helper function to transform a list of attributes into usable JSON.
     *
     * @param attributes The attributes to be transformed.
     * @return The JSON representing the attributes.
     */
    @Override
    public String toJson(final List<Attribute> attributes) throws IOException {
        return _mapper.writeValueAsString(attributes);
    }

    /**
     * A standard helper function to transform an attribute into usable JSON.
     *
     * @param attribute The attribute to be transformed.
     * @return The JSON representing the attribute.
     */
    @Override
    public String toJson(final Attribute attribute) throws IOException {
        return _mapper.writeValueAsString(attribute);
    }

    /**
     * Gets a displayable list of all categories
     *
     * @return A list of the categories.
     */
    public String getCategoriesString() {
        StringBuilder buffer = new StringBuilder();
        Set<String> categories = _attributeMap.keySet();
        for (String category : categories) {
            buffer.append(category).append(", ");
        }
        return buffer.delete(buffer.length() - 2, buffer.length() - 1).toString();
    }

    /**
     * Gets a list of all assessments in the specified category
     *
     * @param category The category for which you want to retrieve assessments.
     * @return A list of the categories.
     */
    @Override
    public String getAssessmentsString(final String category) {
        StringBuilder buffer = new StringBuilder();
        List<String> assessments = getAssessments(category);
        for (String assessment : assessments) {
            buffer.append(assessment).append(", ");
        }
        return buffer.delete(buffer.length() - 2, buffer.length() - 1).toString();
    }

    /**
     * Gets a JSON-formatted list of the data dictionary's categories.
     *
     * @return A JSON-formatted list of the data dictionary's categories.
     */
    @Override
    public String getCategoryMap() throws IOException {
        return _mapper.writeValueAsString(getCategories());
    }

    /**
     * Gets a JSON-formatted map of the data dictionary's assessments, keyed to the assessments' containing categories.
     *
     * @return A JSON-formatted map of the data dictionary's assessments.
     */
    @Override
    public String getAssessmentMap() throws IOException {
        List<String> categories = getCategories();
        Map<String, List<String>> assessments = new LinkedHashMap<String, List<String>>(categories.size());
        for (String category : categories) {
            assessments.put(category, getAssessments(category));
        }
        return _mapper.writeValueAsString(assessments);
    }

    /**
     * Gets the full attributes map as JSON.
     *
     * @return A map of all the attributes as JSON.
     */
    @Override
    public String getAttributeMap() throws IOException {
        return _mapper.writeValueAsString(_attributeMap);
    }

    /**
     * Used to initialize the data dictionary from the properties file once Spring initialization has completed.
     *
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        _log.debug("Initializing simple data dictionary service from JSON property");
        initializeCategoriesFromJson(_properties.getProperty("categories"));
        initializeAssessmentsFromJson(_properties.getProperty("assessments"));
        initializeAttributesFromJson(_properties.getProperty("attributes"));
    }

    private void initializeCategoriesFromJson(final String json) throws IOException {
        _categories.clear();
        _categoryNames.clear();
        final List<Category> categories = _mapper.readValue(json, new TypeReference<List<Category>>() {});
        Collections.sort(categories, COMPARATOR);
        _categories.addAll(categories);
        for (Category category : _categories) {
            _categoryNames.add(category.getName());
        }
    }

    private void initializeAssessmentsFromJson(final String json) throws IOException {
        _assessments.clear();
        final List<Assessment> assessments = _mapper.readValue(json, new TypeReference<List<Assessment>>() {});
        for (Assessment assessment : assessments) {
            final String category = assessment.getCategory();
            if (!_assessments.containsKey(category)) {
                _assessments.put(category, new ArrayList<Assessment>());
                _assessmentNames.put(category, new ArrayList<String>());
            }
            _assessments.get(category).add(assessment);
        }
        for (String category : _assessments.keySet()) {
            final List<Assessment> list = _assessments.get(category);
            Collections.sort(list, COMPARATOR);
            for (Assessment assessment : list) {
                _assessmentNames.get(category).add(assessment.getName());
            }
        }
    }

    private void initializeAttributesFromJson(final String json) throws IOException {
        _attributes.clear();
        _attributesByXsiTypeMap.clear();
        _attributesByXsiTypeFieldMap.clear();
        _attributeMap.clear();

        for (final String category : _categoryNames) {
            final Map<String, Map<String, Attribute>> assessmentMap = new LinkedHashMap<String, Map<String, Attribute>>();
            _attributeMap.put(category, assessmentMap);
            for (final String assessment : _assessmentNames.get(category)) {
                final Map<String, Attribute> attributeMap = new LinkedHashMap<String, Attribute>();
                assessmentMap.put(assessment, attributeMap);
            }
        }

        final List<Attribute> list = _mapper.readValue(json, new TypeReference<List<Attribute>>() {});

        for (Attribute attribute : list) {
            // Stash the attribute in the map of attributes by name.
            _attributes.put(attribute.getName(), attribute);

            // Stash the attribute in the map of attributes by XSI type.
            if (!_attributesByXsiTypeMap.containsKey(attribute.getXsiType())) {
                _attributesByXsiTypeMap.put(attribute.getXsiType(), new Hashtable<String, Attribute>());
                _attributesByXsiTypeFieldMap.put(attribute.getXsiType(), new Hashtable<String, Attribute>());
            }
            _attributesByXsiTypeMap.get(attribute.getXsiType()).put(attribute.getName(), attribute);
            _attributesByXsiTypeFieldMap.get(attribute.getXsiType()).put(attribute.getFieldId(), attribute);
            
            // Stash the attribute in the map of categories and assessments.
            final String category = attribute.getCategory();
            final String assessment = attribute.getAssessment();
            _attributeMap.get(category).get(assessment).put(attribute.getName(), attribute);
    }

        // Lastly, walk all the attribute lists and sort them.
        for (String categoryId : _attributeMap.keySet()) {
            Map<String, Map<String, Attribute>> assessmentMap = _attributeMap.get(categoryId);
            for (String assessmentId : assessmentMap.keySet()) {
                Map<String, Attribute> attributeMap = _attributeMap.get(categoryId).get(assessmentId);
                List<Attribute> attributes = new ArrayList<Attribute>(attributeMap.values());
                Collections.sort(attributes, COMPARATOR);
                attributeMap.clear();
                for (Attribute attribute : attributes) {
                    attributeMap.put(attribute.getName(), attribute);
                }
            }
        }
    }

    private static final Node.NodeComparator COMPARATOR = new Node.NodeComparator();
    private static final Log _log = LogFactory.getLog(SERVICE_NAME);

    @Inject
    @Named("dataDictionary")
    private Properties _properties;

    private final ObjectMapper _mapper = new ObjectMapper() {{
        getDeserializationConfig().set(DeserializationConfig.Feature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
        getDeserializationConfig().set(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        getDeserializationConfig().set(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        getDeserializationConfig().set(DeserializationConfig.Feature.WRAP_EXCEPTIONS, true);
        getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        getSerializationConfig().set(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
    }};
    private final List<Category> _categories = new ArrayList<Category>();
    private final List<String> _categoryNames = new ArrayList<String>();
    private final Map<String, List<Assessment>> _assessments = new HashMap<String, List<Assessment>>();
    private final Map<String, List<String>> _assessmentNames = new HashMap<String, List<String>>();
    private final Map<String, Attribute> _attributes = new Hashtable<String, Attribute>();
    private final Map<String, Map<String, Attribute>> _attributesByXsiTypeMap = new Hashtable<String, Map<String, Attribute>>();
    private final Map<String, Map<String, Attribute>> _attributesByXsiTypeFieldMap = new Hashtable<String, Map<String, Attribute>>();
    private final Map<String, Map<String, Map<String, Attribute>>> _attributeMap = new LinkedHashMap<String, Map<String, Map<String, Attribute>>>();
	
}
