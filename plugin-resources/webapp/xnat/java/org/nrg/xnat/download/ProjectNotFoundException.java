/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

public class ProjectNotFoundException extends Exception {
    private static final long serialVersionUID = 3657745049147685105L;
}