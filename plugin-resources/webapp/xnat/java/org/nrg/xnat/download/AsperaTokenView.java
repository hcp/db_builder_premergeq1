/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.AbstractView;

import com.google.common.collect.ImmutableMap;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class AsperaTokenView extends AbstractView {
    public static final String OPT_TARGET_RATE_KBPS = "target_rate_kbps";
    public static final String OPT_DESTINATION_ROOT = "destination_root";
    public static final String OPT_COOKIE = "cookie";
    private static final String CONTENT_TYPE = "application/x-aspera-token+json";

    private final Logger logger = LoggerFactory.getLogger(AsperaTokenView.class);

    private final URL url;
    private final String auth;
    private final ImmutableMap<String,?> requestOptions, specOptions;

    /**
     * Request a token 
     * @param nodeSite
     * @param nodeUser
     * @param nodePassword
     * @param requestOptions
     * @throws MalformedURLException
     */
    public AsperaTokenView(final URL nodeSite,
            final String nodeUser, final String nodePassword,
            final Map<String,?> requestOptions,
            final Map<String,?> specOptions) throws MalformedURLException {
        this.url = new URL(nodeSite, "files/download_setup");
        this.auth = DatatypeConverter.printBase64Binary((nodeUser + ":" + nodePassword).getBytes());
        this.requestOptions = ImmutableMap.copyOf(requestOptions);
        this.specOptions = ImmutableMap.copyOf(specOptions);
    }

    private final JSONObject addPath(final JSONArray paths, final String path) throws JSONException {
        final JSONObject entry = new JSONObject();
        entry.put("source", path);
        paths.put(entry);
        return entry;
    }

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.view.AbstractView#renderMergedOutputModel(java.util.Map, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void renderMergedOutputModel(final Map<String,Object> model,
            final HttpServletRequest request,
            final HttpServletResponse response) throws IOException,JSONException,URISyntaxException {
        @SuppressWarnings("unchecked")
        final Iterable<Package> packages = (Iterable<Package>)model.get(DownloadPackageService.MODEL_PATHS);

        final JSONArray paths = new JSONArray();        

        for (final Package pkg : packages) {
            for (final Iterator<Path> pathi = pkg.getPathIterator(); pathi.hasNext(); ) {
                final String path = pathi.next().toString();
                logger.trace("adding path {} to Aspera token", path);
                addPath(paths, path);
            }
        }
        final JSONObject xferRequest = new JSONObject();
        xferRequest.put("paths", paths);
        for (final Map.Entry<String,?> opte : requestOptions.entrySet()) {
            xferRequest.put(opte.getKey(), opte.getValue());
        }
        final JSONObject reqwrap = new JSONObject();
        reqwrap.put("transfer_request", xferRequest);
        final JSONArray requests = new JSONArray();
        requests.put(reqwrap);
        final JSONObject entity = new JSONObject();
        entity.put("transfer_requests", requests);

        logger.debug("issuing transfer request with authorization Basic {}: {}", auth, entity);

        final HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Authorization", "Basic " + auth); 
        final OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        try {
            entity.write(writer);
        } finally {
            writer.close();
        }

        final int status = connection.getResponseCode();
        if (2 == status / 100) { // TODO: symbolic constant
            response.setContentType(CONTENT_TYPE);
            final InputStreamReader reader = new InputStreamReader(connection.getInputStream());
            try {
                final JSONTokener tokener = new JSONTokener(reader);
                final JSONObject specs = new JSONObject(tokener);
                logger.debug("Aspera Node response {}: {}", status, specs);
                final JSONArray specsarray = specs.getJSONArray("transfer_specs");
                final JSONObject specwrapper = specsarray.getJSONObject(0);
                final JSONObject spec = specwrapper.getJSONObject("transfer_spec");
                spec.put("authentication", "token");
                for (final Map.Entry<String,?> opte : specOptions.entrySet()) {
                    spec.put(opte.getKey(),  opte.getValue());
                }
                spec.write(response.getWriter());
            } catch (JSONException e) {
                logger.error("Aspera transfer spec handoff failed", e);
            } finally {
                reader.close();
            }
        } else {
            final String message = connection.getResponseMessage();
            logger.error("Aspera Node request failed: {}", message);
            throw new IOException(String.format("Aspera node request failed %d (%s)", status, message));
        }
    }
}
