/**
 * Copyright (c) 2013 Washington University School of Medicine
 */
package org.nrg.xnat.download;

import java.util.Set;

import com.google.common.base.Function;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface PackageBuilderProvider extends Function<String,PackageBuilder> {
    Set<String> getPackageNames();
    String getPackageDescription(String packageName);
    Set<String> getPackageKeywords(String packageName);
    String getPackageLabel(String packageName);
}
