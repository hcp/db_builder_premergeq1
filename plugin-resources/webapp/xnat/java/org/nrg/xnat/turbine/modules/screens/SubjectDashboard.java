package org.nrg.xnat.turbine.modules.screens;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xnat.services.datadictionary.DataDictionaryService;

/**
 * @author tim@deck5consulting.com
 *         <p/>
 * Prep class for the interactive dashboard (Quick View).  Doesn't do anything but basic initialization (security)
 */
public class SubjectDashboard extends SecureScreen {

	@Override
	protected void doBuildTemplate(final RunData data, final Context context) throws Exception {

	final String groupId = StringEscapeUtils.unescapeXml(StringEscapeUtils.unescapeHtml(((String) TurbineUtils
		.GetPassedParameter("subjectGroupName", data))));
	final String subject = StringEscapeUtils.unescapeXml(StringEscapeUtils.unescapeHtml(((String) TurbineUtils
		.GetPassedParameter("subject", data))));
	context.put("autoOpenGroupName", StringEscapeUtils.escapeJava(groupId)); // this variable is going directly into
										 // a JS string, need to escape double
										 // quotes
	context.put("autoOpenSubject", subject);

        DataDictionaryService dictService = XDAT.getContextService().getBean(DataDictionaryService.class);
        context.put("categories", dictService.getCategoryMap());
        context.put("assessments", dictService.getAssessmentMap());
        context.put("attributeMap",dictService.getAttributeMap());
        
        String project;
        if(TurbineUtils.HasPassedParameter("project", data)){
        	project=(String)TurbineUtils.GetPassedParameter("project", data);
        }else{
        	project = StringUtils.trimToEmpty(XDAT.getConfigService().getConfigContents("dashboard", "project"));
        }
        
        if (project.contains(",")) {
        	context.put("project", project);
        }else{
            XnatProjectdata p= XnatProjectdata.getProjectByIDorAlias(project, TurbineUtils.getUser(data), false);
            if(p!=null){
            	context.put("project", p.getId());
            	context.put("om", p);
            }
        }
	}

}
