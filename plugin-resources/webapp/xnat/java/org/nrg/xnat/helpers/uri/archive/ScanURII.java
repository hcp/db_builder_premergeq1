package org.nrg.xnat.helpers.uri.archive;

import org.nrg.xdat.om.XnatImagescandata;

public interface ScanURII {
	public XnatImagescandata getScan();
}
